<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $password = bcrypt('123456');
        $limit = 100;

		// Administrator (User)
        User::create([
        	'name' 			=> 'Administrator',
			'email'             => 'admin@mail.com',
			'phone' 			=> '0711111111',
			'idno' 				=> '11111111',
			'password' 			=> $password,
			'role' 			    => 1,
			'status' 			=> 1,
			'slug' 				=> 'administrator',
			'remember_token' 	=> str_random(10),
			'created_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
		]);
		
		// Author (User)
        User::create([
			'name' 				=> 'Author',
			'email'             => 'author@mail.com',
			'phone' 			=> '0722222222',
			'idno' 				=> '22222222',
			'password' 			=> $password,
			'role' 			    => 0,
			'status' 			=> 1,
			'slug' 				=> 'author',
			'remember_token' 	=> str_random(10),
			'created_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
        ]);

		for ($i = 0; $i < $limit; $i++)
		{
			DB::table('users')->insert([
				'name' 				=> $faker->name,
				'email'             => $faker->unique()->safeEmail,
				'phone' 			=> $faker->e164PhoneNumber,
				'idno' 				=> rand(0720000000, 0723000000),
				'password' 			=> $password,
				'role' 			    => rand(0,1),
				'status' 			=> rand(0,1),
				'slug' 				=> Str::slug($faker->name),
				'remember_token' 	=> str_random(10),
				'created_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' 		=> Carbon::now()->format('Y-m-d H:i:s'),
			]);
		}
    }
}
