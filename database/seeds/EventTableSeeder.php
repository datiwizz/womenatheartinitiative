<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Event;
use App\County;
use App\User;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 150;
        $countyID = County::all()->pluck('id')->toArray();
        $userID = User::all()->pluck('id')->toArray();

        for ($i = 0; $i < $limit; $i++)
        {
            DB::table('events')->insert([
                'title'     => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'content'   => $faker->realText(rand(80, 600)),
                'status'    => rand(0,1),
                'slug'      => Str::slug($faker->sentence($nbWords = 3, $variableNbWords = true)),
                'event_date' => $faker->dateTimeBetween('+1 week', '+1 month'),
                'county_id' => $faker->randomElement($countyID),
                'user_id'   => $faker->randomElement($userID),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
