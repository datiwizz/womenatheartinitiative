<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Category;
use App\County;
use App\User;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 250;
        $categoryID = Category::all()->pluck('id')->toArray();
        $countyID = County::all()->pluck('id')->toArray();
        $userID = User::all()->pluck('id')->toArray();

        for ($i = 0; $i < $limit; $i++)
        {
        	DB::table('posts')->insert([
        		'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'content' => $faker->text($maxNbChars = 2000),
                'status' => rand(0,1),        		
                'slug' => Str::slug($faker->sentence($nbWords = 5, $variableNbWords = true)),
                'county_id' => $faker->randomElement($countyID),
                'category_id' => $faker->randomElement($categoryID),
        		'user_id' => $faker->randomElement($userID),
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        	]);
        }
    }
}
