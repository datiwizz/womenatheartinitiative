<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\County;

class CountyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('counties')->insert([
            [
                'name' => 'Baringo',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 1,
                'slug' => 'baringo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Elgeyo Marakwet',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 0,
                'slug' => 'elgeyo-marakwet',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Nandi',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 0,
                'slug' => 'nandi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Turkana',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 0,
                'slug' => 'turukana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Uasin Gishu',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 1,
                'slug' => 'uasin-gishu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'West Pokot',
                'content'   => $faker->text($maxNbChars = 150),
                'status' => 1,
                'slug' => 'west-pokot',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
  
        ]);
    }
}
