<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Contact;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 150;

        for ($i = 0; $i < $limit; $i++)
        {
            DB::table('contacts')->insert([
            	'name'		=> $faker->name,
            	'email'		=> $faker->unique()->safeEmail,
                'title'     => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'content'   => $faker->text($maxNbChars = 800),
                'status'    => rand(0,1),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
