<?php

namespace App;

use App\County;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //Table
	protected $table = 'events';
	//Fillables
    protected $fillable = [
        'title', 'content', 'status', 'slug', 'county_id', 'user_id', 'event_date', 'bigpic', 'smallpic',
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function county()
    {
        return $this->belongsTo(County::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
