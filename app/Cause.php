<?php

namespace App;

use App\County;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Cause extends Model
{
    //Table
    protected $table = 'causes';
    //Fillables
    protected $fillable = [
        'title', 'content', 'amount', 'raised', 'status', 'county_id', 'user_id', 'bigpic', 'smallpic',
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function county()
    {
        return $this->belongsTo(County::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
