<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //Table
    protected $table = 'comments';
    //Fillables
    protected $fillable = [
        'name', 'content', 'email', 'status', 'post_id', 
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function posts()
    {
        return $this->belongsTo(Post::class);
    }
}
