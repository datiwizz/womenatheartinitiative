<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    //Table
    protected $table = 'newsletters';
    //Fillables
    protected $fillable = [
        'email', 'status',
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
