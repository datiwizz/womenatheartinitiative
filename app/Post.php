<?php

namespace App;

use App\County;
use App\User;
use App\Category;
use App\Comment;
use App\Tag;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table
	protected $table = 'posts';
	//Fillables
    protected $fillable = [
        'title', 'content', 'slug', 'status', 'county_id', 'category_id', 'user_id', 'bigpic', 'smallpic', 
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function county()
    {
        return $this->belongsTo(County::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsTo(Tag::class);
    }

    public function comments()
    {
        return $this->belongsTo(Comment::class);
    }
}
