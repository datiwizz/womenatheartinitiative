<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constitution extends Model
{
    protected $table = 'constitutions';

    protected $fillable = [
        'content', 'attachment',
    ];

    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
