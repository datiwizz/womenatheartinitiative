<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = [
        'name',
    ];

    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
    
    public function posts()
    {
        return $this->belongsTo(Post::class);
    }
}
