<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'name', 'email', 'title', 'content', 'status',
    ];

    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
