<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'abouts';

    protected $fillable = [
        'content',
    ];

    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
