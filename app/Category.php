<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'name',
    ];

    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function posts()
    {
        return $this->belongsTo(Post::class);
    }
}
