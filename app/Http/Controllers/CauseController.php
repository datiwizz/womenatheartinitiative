<?php

namespace App\Http\Controllers;

use DB;
use Image;
use Storage;
use App\Cause;
use App\County;
use App\User;
use Illuminate\Http\Request;

class CauseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Cause::with('county', 'user')->orderBy('id', 'desc')->paginate(25);
        return view('back.cause.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Cause;
        $county = County::where('status', 1)->pluck('name', 'id');
        return view('back.cause.create', compact('data', 'county'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'amount' => 'bail|required|numeric|min:0',
            'raised' => 'bail|min:0',
            'county_id' => 'bail|required',
            'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'amount.required' => 'Amount Needed is required.',
            'amount.numeric' => 'Amount should be numeric.',
            'amount.min' => 'Amount should not be negative.',
            'raised.min' => 'Amount Raised should not be negative.',
            'county_id.required' => 'County is required.',
            'status.required' => 'Status is required.',
            'post_image.image' => 'Upload image must be an image.'
        ]);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/cause/'.$filename);
            $location2 = public_path('images/cause/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);
        } else {
            $filename = 'bigpic.jpg';
            $filename2 = 'smallpic.jpg';
        }

        $post = new Cause;
        $post->title = ucwords(strtolower($request->input('title')));
        $post->amount = $request->input('amount');
        // $post->raised = $request->input('raised');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');
        $post->user_id = 3;
        $post->slug = $this->createSlug($request->title);
        $post->bigpic = $filename;
        $post->smallpic = $filename2;

        if ($post->save()) {
            return redirect()->route('cause.index')->with('status', 'Cause Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cause  $cause
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = Cause::where('slug', $slug)->first();
        $county = County::pluck('name','id');
        return view('back.cause.show', compact('data', 'county'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cause  $cause
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = Cause::where('slug', $slug)->first();
        $county = County::where('status', 1)->pluck('name','id');
        return view('back.cause.edit', compact('data', 'county'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cause  $cause
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'amount' => 'bail|required|numeric|min:0',
            'raised' => 'bail|min:0',
            'county_id' => 'bail|required',
            'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'amount.required' => 'Amount Needed is required.',
            'amount.numeric' => 'Amount should be numeric.',
            'amount.min' => 'Amount should not be negative.',
            'raised.min' => 'Amount Raised should not be negative.',
            'county_id.required' => 'County is required.',
            'status.required' => 'Status is required.',
            'post_image.image' => 'Upload image must be an image.'
        ]);

        $post = Cause::findorFail($id);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/cause/'.$filename);
            $location2 = public_path('images/cause/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);

        }

        $post->title = ucwords(strtolower($request->input('title')));
        $post->amount = $request->input('amount');
        $post->raised = $request->input('raised');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');

        if ($post->slug != $request->slug) {
            $post->slug = $this->createSlug($request->title, $id);
        }
        if ($request->hasFile('post_image')) {
            if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
                unlink(public_path().'/images/cause/'. $post->bigpic);
                unlink(public_path().'/images/cause/'. $post->smallpic);
            }
            $post->bigpic  = $filename;
            $post->smallpic  = $filename2;
        }
        // dd($post);
        if ($post->save()) {
            return redirect()->route('cause.index')->with('status', 'Cause Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cause  $cause
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Cause::findOrFail($id);
        // if(Auth::user()->id !==$post->user_id){
        //     return redirect()->route('post.index')->with('error','Unauthorized Action.');
        // }
        if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
            Storage::delete('cause/'.$post->bigpic);
            Storage::delete('cause/'.$post->smallpic);
        }
        $post->delete();
        return redirect()->route('cause.index')->with('status','Cause successfully deleted.');
    }

    public function createSlug($title, $id = 0)
     {
     // Normalize the title
         $slug = str_slug($title);
     // Get any that could possibly be related.
     // This cuts the queries down by doing it once.
         $allSlugs = $this->getRelatedSlugs($slug, $id);
     // If we haven't used it before then we are all good.
         if (! $allSlugs->contains('slug', $slug)){
             return $slug;
         }
     // Just append numbers like a savage until we find not used.
         for ($i = 1; $i <= 10; $i++) {
             $newSlug = $slug.'-'.$i;
             if (! $allSlugs->contains('slug', $newSlug)) {
                 return $newSlug;
             }
         }
         throw new \Exception('Can not create a unique slug');
     }
     protected function getRelatedSlugs($slug, $id = 0)
     {
         return Cause::select('slug')->where('slug', 'like', $slug.'%')
         ->where('id', '<>', $id)
         ->get();
     }
}
