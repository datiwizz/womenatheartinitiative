<?php

namespace App\Http\Controllers;

use DB;
use App\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = About::all();
        return view('back.about.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new About;
        return view('back.about.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'content' => 'bail|required|min:2',
        ],[
            'content.required' => 'About is required.',
            'content.min' => 'About must be at least 2 characters.',
        ]);

        $post           = new About;
        $post->content  = $request->input('content');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('about.index')->with('status', 'About Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = About::find($id);
      return view('back.about.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'content' => 'bail|required|min:2',
        ],[
            'content.required' => 'About is required.',
            'content.min' => 'About must be at least 2 characters.',
        ]);

        $post           = About::find($id);
        $post->content  = $request->input('content');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('about.index')->with('status', 'About Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }
}
