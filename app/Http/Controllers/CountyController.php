<?php

namespace App\Http\Controllers;

use DB;
use App\County;
use Illuminate\Http\Request;

class CountyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = County::all();
        return view('back.county.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new County;
        return view('back.county.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'bail|required|alpha_spaces|min:2',
          'content' => 'bail|required',
          'status' => 'bail|required',
      ],[
          'name.required' => 'County Name is required.',
          'name.alpha_spaces' => 'County Name may only contain letters and spaces.',
          'name.min' => 'County Name must be at least 2 characters.',
          'content.required' => 'County Description is required.',
          'status.required' => 'Status is required.',
      ]);

      $post           = new County;
      $post->name     = ucwords(strtolower($request->input('name')));
      $post->content  = $request->input('content');
      $post->Status   = $request->input('status');
      $post->slug     = $this->createSlug($request->name);
      // dd($post);
      if ($post->save()) {
          return redirect()->route('county.index')->with('status', 'County Successfully Created');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\County  $county
     * @return \Illuminate\Http\Response
     */
    public function show(County $county)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\County  $county
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = County::where('slug', $slug)->first();
        return view('back.county.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\County  $county
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'name' => 'bail|required|alpha_spaces|min:2',
          'content' => 'bail|required',
          'status' => 'bail|required',
      ],[
          'name.required' => 'County Name is required.',
          'name.alpha_spaces' => 'County Name may only contain letters and spaces.',
          'name.min' => 'County Name must be at least 2 characters.',
          'content.required' => 'County Description is required.',
          'status.required' => 'Status is required.',
      ]);

      $post           = County::find($id);
      $post->name     = ucwords(strtolower($request->input('name')));
      $post->content  = $request->input('content');
      $post->Status   = $request->input('status');
      // $post->slug     = $this->createSlug($request->name);
      if ($post->slug != $request->slug) {
            $post->slug = $this->createSlug($request->name, $id); }
      // dd($post);
      if ($post->save()) {
          return redirect()->route('county.index')->with('status', 'County Successfully Updated');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\County  $county
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = County::findOrFail($id);
      $post->delete();
      return redirect()->route('county.index')->with('status','County Deleted Successfully....');
    }

    public function createSlug($title, $id = 0)
     {
     // Normalize the title
         $slug = str_slug($title);
     // Get any that could possibly be related.
     // This cuts the queries down by doing it once.
         $allSlugs = $this->getRelatedSlugs($slug, $id);
     // If we haven't used it before then we are all good.
         if (! $allSlugs->contains('slug', $slug)){
             return $slug;
         }
     // Just append numbers like a savage until we find not used.
         for ($i = 1; $i <= 10; $i++) {
             $newSlug = $slug.'-'.$i;
             if (! $allSlugs->contains('slug', $newSlug)) {
                 return $newSlug;
             }
         }
         throw new \Exception('Can not create a unique slug');
     }
     protected function getRelatedSlugs($slug, $id = 0)
     {
         return County::select('slug')->where('slug', 'like', $slug.'%')
         ->where('id', '<>', $id)
         ->get();
     }
}
