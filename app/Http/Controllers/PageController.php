<?php

namespace App\Http\Controllers;
use DB;
use Image;
use Storage;
use App\Post;
use App\County;
use App\Contact;
use App\Category;
use App\About;
use App\Cause;
use App\Constitution;
use App\Event;
use App\Newsletter;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
      $cause = Cause::orderBy('id', 'desc')->where('status', '=', '1')->limit(8)->get();
      $cause2 = Cause::inRandomOrder()->where('status', '=', '1')->limit(1)->get();
      $about = About::all();
      $event = Event::orderBy('id', 'desc')->where('status', '=', '1')->limit(2)->get();
      $event2 = Event::inRandomOrder()->where('status', '=', '1')->limit(3)->get();
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      return view('front.index', compact('post', 'about', 'event', 'cause', 'event2', 'cause2'));
    }

    public function aboutus()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $about = About::all();
      return view('front.about', compact('post', 'about'));
    }

    public function ourcauses()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $cause = Cause::orderBy('id', 'desc')->where('status', '=', '1')->paginate(6);
      return view('front.ourcause', compact('post', 'cause'));
    }

    public function singlecause($slug)
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $data = Cause::where('slug', $slug)->firstOrFail();
      return view('front.singlecause', compact('data', 'post'));
    }

    public function ourevents()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $event = Event::orderBy('id', 'desc')->where('status', '=', '1')->paginate(6);
      return view('front.ourevent', compact('post', 'event'));
    }

    public function singleevent($slug)
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $event = Event::where('slug', $slug)->firstOrFail();
      return view('front.singleevent', compact('event', 'post'));
    }

    public function ourblog()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $blog = Post::orderBy('id', 'desc')->where('status', '=', '1')->paginate(4);
      return view('front.ourblog', compact('post', 'blog'));
    }

    public function singleblog($slug)
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $blog = Post::where('slug', $slug)->firstOrFail();
      return view('front.singleblog', compact('blog', 'post'));
    }

    public function contactus()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      return view('front.contactus', compact('post'));
    }

    public function yourdonation()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      return view('front.yourdonation', compact('post'));
    }

    public function ourconstitution()
    {
      $post = Post::orderBy('id', 'desc')->where('status', '=', '1')->limit(3)->get();
      $constitution = Constitution::all();
      return view('front.ourconstitution', compact('post', 'constitution'));
    }

    public function ournewsletter(Request $request)
    {
      $this->validate($request, [
          'email2' => 'bail|required|email|unique:newsletters,email,',
      ],[
        'email2.required' => 'Email Address is required.',
        'email2.email' => 'Email Address must be a valid email.',
        'email2.unique' => 'Email Address is already registered.',
      ]);

      $post = new Newsletter;
      $post->email      = $request->input('email2');
      if ($post->save()) {
          return back()->with('status', 'Thank you for subscribing.');
      }
    }

    public function oursubscribe(Request $request)
    {
      $this->validate($request, [
          'email' => 'bail|required|email|unique:newsletters,email,',
      ],[
        'email.required' => 'Email Address is required.',
        'email.email' => 'Email Address must be a valid email.',
        'email.unique' => 'Email Address is already registered.',
      ]);

      $post = new Newsletter;
      $post->email      = $request->input('email');
      if ($post->save()) {
          return back()->with('status', 'Thank you for subscribing.');
      }
    }

    public function messageus(Request $request)
    {
      $this->validate($request, [
          'name'  => 'bail|required|alpha_spaces|min:2',
          'email' => 'bail|required|email',
          'title' => 'bail|required|alpha_spaces|min:2',
          'content' => 'bail|required',
      ],[
        'name.required' => 'Name is required.',
        'name.alpha_spaces' => 'Name may only contain letters and spaces.',
        'name.min' => 'Name must be at least 2 characters.',
        'email.required'  => 'Email Address is required.',
        'email.email'     => 'Email Address must be a valid email.',
        'title.required' => 'Subject is required.',
        'title.alpha_spaces' => 'Subject may only contain letters and spaces.',
        'title.min' => 'Subject must be at least 2 characters.',
        'content.required' => 'Message is required.',
        // 'content.alpha_num' => 'Message should only contain alpha naumeric values.',
      ]);

      $post = new Contact;
      $post->name = ucwords(strtolower($request->input('name')));
      $post->email = $request->input('email');
      $post->title = ucwords(strtolower($request->input('title')));
      $post->content = strip_tags($request->input('content'));
      if ($post->save()) {
          return back()->with('status', 'Thank you. We will respond to your message as soon as possible.');
      }
    }
}
