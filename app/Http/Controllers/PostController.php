<?php

namespace App\Http\Controllers;

use DB;
use Image;
use Storage;
use Auth;
use App\User;
use App\Post;
use App\County;
use App\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 1) {
            $data = Post::with('county', 'user')->orderBy('id', 'desc')->paginate(25);
            return view('back.post.index', compact('data'));
        } else {
            $data = Post::where("user_id", "=", Auth::user()->id)->with('county', 'user')->orderBy('id', 'desc')->paginate(25);
            return view('back.post.index', compact('data'));
        }        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Post;
        $category = Category::pluck('name', 'id');
        $county = County::where('status', '=', '1')->pluck('name','id');
        return view('back.post.create', compact('data', 'county', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'category_id' => 'bail|required',
            'county_id' => 'bail|required',
            // 'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'category_id.required' => 'Category is required.',
            'county_id.required' => 'County is required.',
            // 'status.required' => 'Status is required.',
        ]);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/blog/'.$filename);
            $location2 = public_path('images/blog/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);

        } else {
            $filename = 'bigpic.jpg';
            $filename2 = 'smallpic.jpg';
        }

        $post = new Post;
        $post->title = ucwords(strtolower($request->input('title')));
        $post->category_id = $request->input('category_id');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');
        $post->user_id = Auth::user()->id;
        $post->slug = $this->createSlug($request->title);
        $post->bigpic = $filename;
        $post->smallpic = $filename2;

        // dd($post);
        if ($post->save()) {
            return redirect()->route('post.index')->with('status', 'Post Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = Post::where('slug', $slug)->first();
        $category = Category::pluck('name', 'id');
        $county = County::where('status', '=', '1')->pluck('name','id');
        return view('back.post.show', compact('data', 'county', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = Post::where('slug', $slug)->first();
        $category = Category::pluck('name', 'id');
        $county = County::where('status', '=', '1')->pluck('name','id');
        return view('back.post.edit', compact('data', 'county', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'category_id' => 'bail|required',
            'county_id' => 'bail|required',
            'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'category_id.required' => 'Category is required.',
            'county_id.required' => 'County is required.',
            'status.required' => 'Status is required.',
        ]);

        $post = Post::findorFail($id);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/blog/'.$filename);
            $location2 = public_path('images/blog/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);

        }

        $post->title = ucwords(strtolower($request->input('title')));
        $post->category_id = $request->input('category_id');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');

        if ($post->slug != $request->slug) {
            $post->slug = $this->createSlug($request->title, $id);
        }
        if ($request->hasFile('post_image')) {
            if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
                unlink(public_path().'/images/blog/'. $post->bigpic);
                unlink(public_path().'/images/blog/'. $post->smallpic);
            }
            $post->bigpic  = $filename;
            $post->smallpic  = $filename2;
        }
        // dd($post);
        if ($post->save()) {
            return redirect()->route('post.index')->with('status', 'Post Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        // if(Auth::user()->id !==$post->user_id){
        //     return redirect()->route('post.index')->with('error','Unauthorized Action.');
        // }
        if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
            Storage::delete('blog/'.$post->bigpic);
            Storage::delete('blog/'.$post->smallpic);
        }
        $post->delete();
        return redirect()->route('post.index')->with('status','Post successfully deleted.');
    }

    public function createSlug($title, $id = 0)
     {
     // Normalize the title
         $slug = str_slug($title);
     // Get any that could possibly be related.
     // This cuts the queries down by doing it once.
         $allSlugs = $this->getRelatedSlugs($slug, $id);
     // If we haven't used it before then we are all good.
         if (! $allSlugs->contains('slug', $slug)){
             return $slug;
         }
     // Just append numbers like a savage until we find not used.
         for ($i = 1; $i <= 10; $i++) {
             $newSlug = $slug.'-'.$i;
             if (! $allSlugs->contains('slug', $newSlug)) {
                 return $newSlug;
             }
         }
         throw new \Exception('Can not create a unique slug');
     }
     protected function getRelatedSlugs($slug, $id = 0)
     {
         return Post::select('slug')->where('slug', 'like', $slug.'%')
         ->where('id', '<>', $id)
         ->get();
     }
}
