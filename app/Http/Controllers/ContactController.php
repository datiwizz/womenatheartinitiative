<?php

namespace App\Http\Controllers;

use DB;
use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data = Contact::orderBy('id', 'desc')->simplePaginate(50);
    	return view('back.contact.index', compact('data'));
    }

    public function edit($id)
    {
        $data = Contact::find($id);
        return view('back.contact.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'status' => 'bail|required',
        ],[
            'status.required' => 'Status is required.',
        ]);

        $post           	= Contact::find($id);
        $post->status     = $request->input('status');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('contact.index')->with('status', 'Message Successfully Updated');
        }
    }

    public function destroy($id)
    {
      $post = Contact::findOrFail($id);
      $post->delete();
      return redirect()->route('contact.index')->with('status','Message Deleted Successfully....');
    }
}
