<?php

namespace App\Http\Controllers;

use DB;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Category::all();
        return view('back.category.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Category;
        return view('back.category.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'bail|required|alpha_spaces|min:2',
        ],[
            'name.required' => 'Category Name is required.',
            'name.alpha_spaces' => 'Category Name may only contain letters and spaces.',
            'name.min' => 'Category Name must be at least 2 characters.',
        ]);

        $post           = new Category;
        $post->name     = ucwords(strtolower($request->input('name')));
        // dd($post);
        if ($post->save()) {
            return redirect()->route('category.index')->with('status', 'Category Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::find($id);
        return view('back.category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'bail|required|alpha_spaces|min:2',
        ],[
            'name.required' => 'Category Name is required.',
            'name.alpha_spaces' => 'Category Name may only contain letters and spaces.',
            'name.min' => 'Category Name must be at least 2 characters.',
        ]);

        $post           = Category::find($id);
        $post->name     = ucwords(strtolower($request->input('name')));
        // dd($post);
        if ($post->save()) {
            return redirect()->route('category.index')->with('status', 'Category Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Category::findOrFail($id);
      $post->delete();
      return redirect()->route('category.index')->with('status','Category Deleted Successfully....');
    }
}
