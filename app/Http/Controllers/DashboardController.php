<?php

namespace App\Http\Controllers;

use DB;
use Image;
use Auth;
use Gate;
use Storage;
use App\Post;
use App\County;
use App\Category;
use App\About;
use App\Cause;
use App\Constitution;
use App\Event;
use App\Newsletter;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $causesum = Cause::all();
        $postsum = Post::all();
        $countysum = County::all();
        $eventsum = Event::all();
        $event = Event::orderBy('id', 'desc')->where('status', '=', '1')->limit(4)->get();
        $cause = Cause::orderBy('id', 'desc')->where('status', '=', '1')->limit(4)->get();
        return view('back.dashboard', compact('causesum', 'postsum', 'countysum', 'eventsum', 'event', 'cause'));
    }
}
