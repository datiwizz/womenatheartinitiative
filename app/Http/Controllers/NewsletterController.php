<?php

namespace App\Http\Controllers;

use DB;
use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Newsletter::orderBy('id', 'desc')->paginate(25);
        return view('back.newsletter.index', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Newsletter::find($id);
        return view('back.newsletter.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'email' => 'bail|required|email|unique:newsletters,email,'.$id,
          'status' => 'bail|required',
        ],[
            'email.required' => 'Email Address is required.',
            'email.email' => 'Email Address must be a valid email.',
            'email.unique' => 'Email Address is already registered.',
            'status.required' => 'User Status is required.',
        ]);

        $post           = Newsletter::find($id);
        $post->email     = $request->input('email');
        $post->status = $request->input('status');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('newsletter.index')->with('status', 'Newsletter Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Newsletter::findOrFail($id);
        $post->delete();
        return redirect()->route('newsletter.index')->with('status','Newsletter Deleted Successfully....');
    }
}
