<?php

namespace App\Http\Controllers;

use Gate;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('isAdmin')) {
            return redirect()->back()->with('status', 'Sorry, the page is not accessible');
        }
        $data = User::orderBy('id', 'desc')->paginate(25);
        return view('back.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new User;
        return view('back.user.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|alpha_spaces|min:2',
            'email' => 'bail|required|email|unique:users,email,',
            'phone' => 'bail|required|numeric|unique:users,phone,',
            'idno' => 'bail|required|numeric|unique:users,idno,',
            'role' => 'bail|required',
            'status' => 'bail|required',
            'password' => 'bail|required',
        ],[
            'name.required' => 'Fullnames are required.',
            'name.alpha_spaces' => 'Fullnames may only contain letters and spaces.',
            'name.min' => 'Fullnames must be at least 2 characters.',
            'email.required' => 'Email Address is required.',
            'email.email' => 'Email Address must be a valid email.',
            'email.unique' => 'Email Address is already registered.',
            'phone.required' => 'Phone Number is required.',
            'phone.numeric' => 'Phone Number must be numeric.',
            'phone.unique' => 'Phone Number is already registered.',
            'idno.required' => 'ID Number is required.',
            'idno.numeric' => 'ID Number must be numeric.',
            'idno.unique' => 'ID Number is already registered.',
            'role.required' => 'User Role is required.',
            'status.required' => 'User Status is required.',
            'password.required' => 'Password is required.',
        ]);

        $post = new User;
        $post->name = ucwords(strtolower($request->input('name')));
        $post->email = $request->input('email');
        $post->phone = $request->input('phone');
        $post->idno = $request->input('idno');
        $post->role = $request->input('role');
        $post->status = $request->input('status');
        $post->password = Hash::make($request->input('password'));
        $post->slug = $this->createSlug($request->name);
        // dd($post);
        if ($post->save()) {
            return redirect()->back()->with('status', 'User Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = User::where('slug', $slug)->firstOrFail();
        return view('back.user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'bail|required|alpha_spaces|min:2',
            'email' => 'bail|required|email|unique:users,email,'.$id,
            'phone' => 'bail|required|numeric|unique:users,phone,'.$id,
            'idno' => 'bail|required|numeric|unique:users,idno,'.$id,
            'role' => 'bail|required',
            'status' => 'bail|required',
        ],[
            'name.required' => 'Fullnames are required.',
            'name.alpha_spaces' => 'Fullnames may only contain letters and spaces.',
            'name.min' => 'Fullnames must be at least 2 characters.',
            'email.required' => 'Email Address is required.',
            'email.email' => 'Email Address must be a valid email.',
            'email.unique' => 'Email Address is already registered.',
            'phone.required' => 'Phone Number is required.',
            'phone.numeric' => 'Phone Number must be numeric.',
            'phone.unique' => 'Phone Number is already registered.',
            'idno.required' => 'ID Number is required.',
            'idno.numeric' => 'ID Number must be numeric.',
            'idno.unique' => 'ID Number is already registered.',
            'role.required' => 'User Role is required.',
            'status.required' => 'User Status is required.',
        ]);

        $post = User::find($id);
        if (trim($request->password) == '') {
            $post->name = ucwords(strtolower($request->input('name')));
            $post->email = $request->input('email');
            $post->phone = $request->input('phone');
            $post->idno = $request->input('idno');
            $post->role = $request->input('role');
            $post->status = $request->input('status');
            if ($post->slug != $request->slug) {
                $post->slug = $this->createSlug($request->name, $id);
            }
        } else {
            $post->name = ucwords(strtolower($request->input('name')));
            $post->email = $request->input('email');
            $post->phone = $request->input('phone');
            $post->idno = $request->input('idno');
            $post->role = $request->input('role');
            $post->password = Hash::make($request->input('password'));
            $post->status = $request->input('status');
            if ($post->slug != $request->slug) {
                $post->slug = $this->createSlug($request->name, $id);
            }
        }
        // dd($post);
        if ($post->save()) {
            return redirect()->back()->with('status', 'Profile Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = User::findOrFail($id);
        $post->posts()->delete();
        $post->delete();
        return redirect()->route('user.index')->with('status', 'User and Related Post Deleted Successfully....');
    }

    public function createSlug($title, $id = 0)
     {
     // Normalize the title
         $slug = str_slug($title);
     // Get any that could possibly be related.
     // This cuts the queries down by doing it once.
         $allSlugs = $this->getRelatedSlugs($slug, $id);
     // If we haven't used it before then we are all good.
         if (! $allSlugs->contains('slug', $slug)){
             return $slug;
         }
     // Just append numbers like a savage until we find not used.
         for ($i = 1; $i <= 10; $i++) {
             $newSlug = $slug.'-'.$i;
             if (! $allSlugs->contains('slug', $newSlug)) {
                 return $newSlug;
             }
         }
         throw new \Exception('Can not create a unique slug');
     }
     protected function getRelatedSlugs($slug, $id = 0)
     {
         return User::select('slug')->where('slug', 'like', $slug.'%')
         ->where('id', '<>', $id)
         ->get();
     }
}
