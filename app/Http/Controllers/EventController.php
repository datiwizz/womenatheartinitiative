<?php

namespace App\Http\Controllers;

use DB;
use Storage;
use Image;
use App\Event;
use App\County;
use App\User;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Event::with('county', 'user')->orderBy('id', 'desc')->paginate(25);
        return view('back.event.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Event;
        $county = County::where('status', 1)->pluck('name', 'id');
        return view('back.event.create', compact('data', 'county'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'event_date' => 'bail|required|date',
            'county_id' => 'bail|required',
            'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'event_date.required' => 'Event date is required.',
            'event_date.date' => 'Event date must be a valid date.',
            'county_id.required' => 'County is required.',
            'status.required' => 'Status is required.',
            'post_image.image' => 'Upload image must be an image.'
        ]);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/event/'.$filename);
            $location2 = public_path('images/event/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);
        } else {
            $filename = 'bigpic.jpg';
            $filename2 = 'smallpic.jpg';
        }

        $post = new Event;
        $post->title = ucwords(strtolower($request->input('title')));
        $post->event_date = $request->input('event_date');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');
        $post->user_id = 3;
        $post->slug = $this->createSlug($request->title);
        $post->bigpic = $filename;
        $post->smallpic = $filename2;

        if ($post->save()) {
            return redirect()->route('event.index')->with('status', 'Event Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = Event::where('slug', $slug)->first();
        $county = County::pluck('name','id');
        return view('back.event.show', compact('data', 'county'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = Event::where('slug', $slug)->first();
        $county = County::where('status', 1)->pluck('name','id');
        return view('back.event.edit', compact('data', 'county'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'bail|required|alpha_spaces|min:2',
            'event_date' => 'bail|required|date',
            'county_id' => 'bail|required',
            'status' => 'bail|required',
            'post_image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'content' => 'bail|required',
        ], [
            'title.required' => 'Title is required.',
            'title.alpha_spaces' => 'Title may only contain letters and spaces.',
            'title.min' => 'Title must be at least 2 characters.',
            'content.required' => 'Post content is required.',
            'event_date.required' => 'Event date is required.',
            'event_date.date' => 'Event date must be a valid date.',
            'county_id.required' => 'County is required.',
            'status.required' => 'Status is required.',
            'post_image.image' => 'Upload image must be an image.'
        ]);

        $post = Event::findorFail($id);

        if ($request->hasFile('post_image')) {
            $image = $request->file('post_image');
            $filename = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $filename2 = uniqid().'_'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/event/'.$filename);
            $location2 = public_path('images/event/'.$filename2);
            Image::make($image)->resize(800, 400)->save($location);
            Image::make($image)->resize(270, 200)->save($location2);

        }

        $post->title = ucwords(strtolower($request->input('title')));
        $post->event_date = $request->input('event_date');
        $post->county_id = $request->input('county_id');
        $post->Status = $request->input('status');
        $post->content = $request->input('content');
        
        if ($post->slug != $request->slug) {
            $post->slug = $this->createSlug($request->title, $id);
        }
        if ($request->hasFile('post_image')) {
            if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
                unlink(public_path().'/images/cause/'. $post->bigpic);
                unlink(public_path().'/images/cause/'. $post->smallpic);
            }
            $post->bigpic  = $filename;
            $post->smallpic  = $filename2;
        }        

        if ($post->save()) {
            return redirect()->route('event.index')->with('status', 'Event Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Event::findOrFail($id);
        // if(Auth::user()->id !==$post->user_id){
        //     return redirect()->route('post.index')->with('error','Unauthorized Action.');
        // }
        if(!($post->bigpic == 'bigpic.jpg' && $post->smallpic == 'smallpic.jpg')){
            Storage::delete('event/'.$post->bigpic);
            Storage::delete('event/'.$post->smallpic);
        }
        $post->delete();
        return redirect()->route('event.index')->with('status','Event successfully deleted.');
    }

    public function createSlug($title, $id = 0)
     {
     // Normalize the title
         $slug = str_slug($title);
     // Get any that could possibly be related.
     // This cuts the queries down by doing it once.
         $allSlugs = $this->getRelatedSlugs($slug, $id);
     // If we haven't used it before then we are all good.
         if (! $allSlugs->contains('slug', $slug)){
             return $slug;
         }
     // Just append numbers like a savage until we find not used.
         for ($i = 1; $i <= 10; $i++) {
             $newSlug = $slug.'-'.$i;
             if (! $allSlugs->contains('slug', $newSlug)) {
                 return $newSlug;
             }
         }
         throw new \Exception('Can not create a unique slug');
     }
     protected function getRelatedSlugs($slug, $id = 0)
     {
         return Event::select('slug')->where('slug', 'like', $slug.'%')
         ->where('id', '<>', $id)
         ->get();
     }
}
