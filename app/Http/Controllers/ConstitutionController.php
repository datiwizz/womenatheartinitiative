<?php

namespace App\Http\Controllers;

use DB;
use App\Constitution;
use Illuminate\Http\Request;

class ConstitutionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Constitution::all();
        return view('back.constitution.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Constitution;
        return view('back.constitution.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'content' => 'bail|required|min:2',
        ],[
            'content.required' => 'Constitution is required.',
            'content.min' => 'Constitution must be at least 2 characters.',
        ]);

        $post           = new Constitution;
        $post->content  = $request->input('content');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('constitution.index')->with('status', 'Constitution Successfully Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Constitution  $constitution
     * @return \Illuminate\Http\Response
     */
    public function show(Constitution $constitution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Constitution  $constitution
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Constitution::find($id);
      return view('back.constitution.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Constitution  $constitution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'content' => 'bail|required|min:2',
        ],[
            'content.required' => 'Constitution is required.',
            'content.min' => 'Constitution must be at least 2 characters.',
        ]);

        $post           = Constitution::find($id);
        $post->content  = $request->input('content');
        // dd($post);
        if ($post->save()) {
            return redirect()->route('constitution.index')->with('status', 'Constitution Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Constitution  $constitution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Constitution $constitution)
    {
        //
    }
}
