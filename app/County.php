<?php

namespace App;

use App\Post;
use App\Event;
use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    //Table
    protected $table = 'counties';
    //Fillables
    protected $fillable = [
        'name', 'content', 'status', 'slug',
    ];
    //primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function events()
    {
        return $this->belongsTo(Event::class);
    }
}
