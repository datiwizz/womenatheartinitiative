<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', ['as'=>'index', 'uses'=>'PageController@index']);
Route::get('/about-us', ['as'=>'aboutus', 'uses'=>'PageController@aboutus']);
Route::get('/our-causes/{cause}', ['as'=>'singlecause', 'uses'=>'PageController@singlecause']);
Route::get('/our-causes', ['as'=>'ourcauses', 'uses'=>'PageController@ourcauses']);
Route::get('/our-events/{event}', ['as'=>'singleevent', 'uses'=>'PageController@singleevent']);
Route::get('/our-events', ['as'=>'ourevents', 'uses'=>'PageController@ourevents']);
Route::get('/our-blog/{blog}', ['as'=>'singleblog', 'uses'=>'PageController@singleblog']);
Route::get('/our-blog', ['as'=>'ourblog', 'uses'=>'PageController@ourblog']);
Route::get('/contact-us', ['as'=>'contactus', 'uses'=>'PageController@contactus']);
Route::get('/your-donation', ['as'=>'yourdonation', 'uses'=>'PageController@yourdonation']);
Route::get('/our-constitution', ['as' => 'ourconstitution', 'uses' => 'PageController@ourconstitution']);
Route::post('/our-newsletter', ['as' => 'ournewsletter', 'uses' => 'PageController@ournewsletter']);
Route::post('/our-subscribe', ['as' => 'oursubscribe', 'uses' => 'PageController@oursubscribe']);
Route::post('/message-us', ['as'=>'messageus', 'uses'=>'PageController@messageus']);

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('post', 'PostController');
Route::resource('user', 'UserController');

// Auth::routes();

// Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
// Route::resource('county', 'CountyController');
// Route::resource('category', 'CategoryController');
// Route::resource('constitution', 'ConstitutionController');
// Route::resource('about', 'AboutController');
// Route::resource('event', 'EventController');
// Route::resource('cause', 'CauseController');
// Route::resource('newsletter', 'NewsletterController');
// Route::resource('contact', 'ContactController');



Route::group(['middleware' => 'can:isAdmin'], function() {
    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    Route::resource('county', 'CountyController');
    Route::resource('category', 'CategoryController');
    Route::resource('constitution', 'ConstitutionController');
    Route::resource('about', 'AboutController');
    Route::resource('event', 'EventController');
    Route::resource('cause', 'CauseController');
    Route::resource('newsletter', 'NewsletterController');
    Route::resource('contact', 'ContactController');
});