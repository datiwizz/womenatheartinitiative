# Women at Heart Initiative

Simple NGO Website Application build with love for learning and laravel 5.6.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisities

- PHP >= 5.6
- MySQL

### Installing

1. Clone this repo: git clone https://datiwizz@bitbucket.org/datiwizz/womenatheartinitiative.git

2. Install dependency
```
composer install --prefer-dist
```

3. Setup Database

4. Setup Aplications

Rename .env.example to .env and fill the environment variable.

Generate a new key for your local application
```
php artisan key:generate
```

Migrate the table to database
```
php artisan migrate
```

Seed User data
```
php artisan db:seed
```

View your application in browser

Admin Login:

```
Email: admin@mail.com
Passw: 123456
```

Author Login:

```
Email: author@mail.com
Passw: 123456
```

## Authors

* **Michael Barasa** - *Initial work*
* **Nelson Ameyo** - *Security, Code Review, Hosting*

## License

This project is licensed under [Sodium Africa](https://sodium.co.ke) , DONT mess it up.