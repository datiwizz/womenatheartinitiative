@extends('front.layout.main')
@section('title', 'Events')
@section('content')

  <!--Page Title-->
<section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
  <div class="auto-container">
      <div class="row clearfix">
          <!--Title -->
          <div class="title-column col-md-6 col-sm-8 col-xs-12">
              <h1>Events</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li class="active">Events list</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container">
  <div class="auto-container">
      <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
      <div class="events-list">
           @if ($event->count()>0)
        @foreach ($event as $value)

                    <!--Event Block Three-->
                    <div class="event-block-three">
                        <div class="inner-box">
                          <div class="row clearfix">
                                <div class="image-column col-md-5 col-sm-4 col-xs-12">
                                    <div class="image">
                                        <img src="{{ asset('images/event/'.$value->smallpic) }}" alt="" />
                                        <a href="/our-events/{{ $value->slug }}" class="overlay-box"><span class="icon flaticon-unlink"></span></a>
                                    </div>
                                </div>
                                <div class="content-column col-md-7 col-sm-8 col-xs-12">
                                    <div class="inner-column">
                                      <div class="post-info">{{ date('F d, Y', strtotime($value->event_date)) }} / <span class="theme_color">{{ $value->county->name }}</span></div>
                                        <h3><a href="/our-events/{{ $value->slug }}">{!! str_limit($value->title, 25) !!}</a></h3>
                                        <div class="text">{!! str_limit($value->content, 225) !!}</div>
                                        <div class="btns-box">
                                            <a href="/our-events/{{ $value->slug }}" class="theme-btn btn-style-four">More Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endforeach
      @else
        <div style="color:#ff0000">No Event has been posted yet!</div>
      @endif
                    <!--Styled Pagination-->
                    <div class="styled-pagination">
                        {{ $event->links() }}
                    </div>
                    <!--End Styled Pagination-->

                </div>
            </div>

            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
             @include('front.layout.sidebar')
            </div>

        </div>
    </div>
</div>


@endsection
