<aside class="sidebar default-sidebar">

                       <!--Category Blog-->
                       <div class="sidebar-widget categories-blog">
                         <div class="sidebar-title">
                             <h2>Categories</h2>
                           </div>
                           <div class="inner-box">
                               <ul>
                                   <li><a href="#">Education <span>15</span></a></li>
                                   <li><a href="#">Foods <span>24</span></a></li>
                                   <li><a href="#">Homeless <span>30</span></a></li>
                                   <li><a href="#">Clean Water <span>45</span></a></li>
                                   <li><a href="#">Cloth <span>18</span></a></li>
                               </ul>
                           </div>
                       </div>

                       <!--Newsletter Widget-->
                       <div class="sidebar-widget newsletter-widget">
                         <div class="inner-box">
                             <div class="sidebar-title">
                                   <h2>News Letter</h2>
                               </div>
                               <div class="text">Make our dreams come true. Subscribe to our mailing list.</div>
                              @if (session('status'))
          <small style="color: green;"><strong>{{ session('status') }}</strong></small>
          @endif
                               <!-- Search -->
                               <div class="sidebar-newsletter-box">
                                   <form method="post" action="{{ route('oursubscribe') }}">
                                    @csrf
                                       <div class="form-group">
                                           <input type="email" name="email" value="" placeholder="Email....." required>
                                           <button type="submit"><span class="icon flaticon-symbol"></span></button>
                                       </div>
                                       @if($errors->has('email'))
            <div class="invalid-feedback">
            <small style="color: red;">{{ $errors->first('email') }}</small>
            </div>
            @endif()
                                   </form>
                               </div>

                           </div>
                       </div>

                   </aside>