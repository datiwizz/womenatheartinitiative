    <!-- Main Header-->
    <header class="main-header header-style-two">

        <!-- Header Top Two-->
        <div class="header-top-two">
            <div class="auto-container">
                <div class="clearfix">

                    <!--Top Left-->
                    <div class="top-left">
                        <!--social-icon-->
                        <div class="social-icon">
                            <ul class="clearfix">
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Header Top End -->

        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                <div class="clearfix">

                    <div class="pull-left logo-outer">
                        <div class="logo"><a href="{{ url('') }}"><img src="{{ asset('images/logo.png') }}" alt="" title=""></a></div>
                    </div>

                    <div class="pull-right upper-right clearfix">

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-technology-1"></span><strong>Phone.</strong></li>
                                <li>(+254) 724-124-569</li>
                            </ul>
                        </div>

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-symbol"></span><strong>Email</strong></li>
                                <li>info@womenatheartinitiative.org</li>
                            </ul>
                        </div>

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-location-pin"></span><strong>Location</strong></li>
                                <li>Eldoret, Kenya.</li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!--Header Lower-->
        <div class="header-lower">
            <div class="auto-container">
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li {{ (Request::is('/') ? 'class=current' : '') }}><a href="{{ url('') }}">Home</a></li>
                                <li {{ (Request::is('*about-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('about-us') }}">About Us</a>
                                </li>
                                <li {{ (Request::is('*our-causes') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-causes') }}">Causes</a>
                                </li>
                                <li {{ (Request::is('*our-events') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-events') }}">Events</a>
                                </li>
                                <li {{ (Request::is('*our-blog') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-blog') }}">Blog</a>
                                </li>
                                <li {{ (Request::is('*contact-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('contact-us') }}">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    @if (Route::has('login'))
                        <div class="outer-box">
                            @auth
                                @can('isAdmin')
                                    <a href="{{ url('/dashboard') }}" class="theme-btn btn-style-one">Dashboard</a>
                                @elsecan('isAuthor')
                                    <a href="{{ url('/post') }}" class="theme-btn btn-style-one">My Post</a>
                                @endcan
                            @else
                                <a href="{{ route('login') }}" class="theme-btn btn-style-one">Login</a>
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!--End Header Lower-->

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="{{ url('') }}" class="img-responsive" title="Tali"><img src="{{ asset('images/logo-small.png') }}" alt="" title=""></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li {{ (Request::is('/') ? 'class=current' : '') }}><a href="{{ url('') }}">Home</a></li>
                                <li {{ (Request::is('*about-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('about-us') }}">About Us</a>
                                </li>
                                <li {{ (Request::is('*our-causes') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-causes') }}">Causes</a>
                                </li>
                                <li {{ (Request::is('*our-events') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-events') }}">Events</a>
                                </li>
                                <li {{ (Request::is('*our-blog') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-blog') }}">Blog</a>
                                </li>
                                <li {{ (Request::is('*contact-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('contact-us') }}">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>

            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->