@include('front.layout.header')

<!-- Preloader -->
<div class="preloader"></div>

@include('front.layout.navigation')

@yield('content')

@include('front.layout.footer')
