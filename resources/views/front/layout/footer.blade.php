 <!--Subscribe Section-->
 <section class="subscribe-section" style="background-image:url({{ asset('images/background/pattern-1.png') }})">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="title-column col-md-6 col-sm-12 col-xs-12">
                <h2>  Subscribe To Our Newsletter</h2>
            </div>
            <div class="form-column col-md-6 col-sm-12 col-xs-12">
                @if (session('status'))
          <small style="color: green;"><strong>{{ session('status') }}</strong></small>
          @endif
                <!--Subscribe Section-->
                <div class="subscribe-form">
                    <form method="post" action="{{ route('ournewsletter') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email2" value="" placeholder="Enter your E-mail" required>
                            <button type="submit" class="theme-btn btn-style-two">Subscribe</button>
                        </div>
                    @if($errors->has('email2'))
            <div class="invalid-feedback">
            <small style="color: red;">{{ $errors->first('email2') }}</small>
            </div>
            @endif()
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<!--End Subscribe Section-->

<!--Main Footer-->
<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!--Footer Column-->
                <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <div class="logo">
                            <a href="{{ url('') }}"><img src="{{ asset('images/footer-logo.png') }}" alt="" /></a>
                        </div>
                        <div class="text">Core values are the fundamental beliefs of a person or organization. The core values are the guiding prin ples that dictate behavior and action suas labore saperet has there any quote for write lorem percit latineu.<br /><br /></div>
                        <ul class="list-style-two">
                            <li><span class="icon flaticon-location-pin"></span>148-50300 Eldoret, Kenya.</li>
                            <li><span class="icon flaticon-technology-1"></span>(+254) 724-124-569</li>
                            <li><span class="icon flaticon-symbol"></span>info@womenatheartinitiative.org</li>
                        </ul>
                    </div>
                </div>

                <!--Footer Column-->
                <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget info-widget">
                        <h2>USE FULL LINKS</h2>
                        <div class="row clearfix">
                            <div class="column col-md-6 col-sm-6 col-xs-12">
                                <ul class="footer-list">
                                    <li><a href="{{ url('') }}">Home</a></li>
                                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li><a href="{{ url('our-causes') }}">Causes</a></li>
                                    <li><a href="{{ url('our-events') }}">Events</a></li>
                                    <li><a href="{{ url('our-blog') }}">Blog</a></li>
                                    <li><a href="{{ url('contact-us') }}">Contact us </a></li>
                                    <li><a href="{{ url('our-constitution') }}">Constitution</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <!--Footer Column-->
                <div class="footer-column col-md-4 col-sm-12 col-xs-12">
                    <div class="footer-widget news-widget">
                        <h2>Latest News</h2>
                        @if ($post->count()>0)
        @foreach ($post as $value)
                        <article class="post">
                            <figure class="post-thumb"><a href="#"><img src="{{ asset('images/blog/'.$value->smallpic) }}" alt=""></a></figure>
                            <div class="text"><a href="#">{!! str_limit($value->title, 15) !!}</a></div>
                            <div class="post-info">{{ date('F d, Y', strtotime($value->created_at)) }} / {{ $value->category->name }}</div>
                        </article>

                        @endforeach
      @else
        <div style="color:#ff0000">No Post has been posted yet!</div>
      @endif

                    </div>
                </div>

            </div>
        </div>

   </div>
   <!--Footer Bottom-->
   <div class="footer-bottom">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-8 col-xs-12">
                    <div class="copyright">Copyright &copy; {!! date('Y') !!} Women at Heart Initiative. All rights reserved</div>
                </div>
                <div class="social-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="social-icon-one style-two">
                        <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
   </div>
</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


<script src="{{ asset('js/jquery.js') }}"></script>
<!--Revolution Slider-->
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{ asset('js/main-slider-script.js')}}"></script>

<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/isotope.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox-media.js')}}"></script>
<script src="{{ asset('js/owl.js')}}"></script>
<script src="{{ asset('js/wow.js')}}"></script>
<script src="{{ asset('js/appear.js')}}"></script>
<script src="{{ asset('js/script.js')}}"></script>
@yield('script')
</body>

</html>