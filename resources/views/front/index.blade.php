@extends('front.layout.main')
@section('title', 'Home')
@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container-fluid" style="padding-top: 95px !important">
        <h1 class="display-4">We need your support</h1>
        <p class="lead">Education, Feed and Accomodate!</p>
        <div class="btns-box" style="transition: none; line-height: 27px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 15px;">
            <a href="/contact-us" class="theme-btn btn-style-two" style="transition: none; text-align: inherit; line-height: 24px; border-width: 2px; margin: 0px 20px 0px 0px; padding: 10px 20px 8px; letter-spacing: 0px; font-weight: 400; font-size: 18px;">Become a Volunteer</a> <a href="/your-donation" class="theme-btn btn-style-five" style="transition: none; text-align: inherit; line-height: 24px; border-width: 2px; margin: 0px 20px 0px 0px; padding: 9px 18px 8px; letter-spacing: 0px; font-weight: 500; font-size: 18px;">Donate</a>
        </div>
    </div>
</div>

<!--Services Section-->
<section class="services-section">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="icon-box"><span class="flaticon-coins"></span></div>
                        <h3><a href="#">Donation</a></h3>
                        <div class="title">Help Drive Change</div>
                    </div>
                    <div class="text">You may feel like a drop in the bucket. But every drop counts!... </div>
                </div>
            </div>

            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="icon-box"><span class="flaticon-money-bag-1"></span></div>
                        <h3><a href="#">Fundrising</a></h3>
                        <div class="title">For a specific cause</div>
                    </div>
                    <div class="text">Be a part of the breakthrough and make someone’s dream come true.... </div>
                </div>
            </div>

            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="icon-box"><span class="flaticon-blood-donation"></span></div>
                        <h3><a href="#">Volunteer</a></h3>
                        <div class="title">By helping other in need</div>
                    </div>
                    <div class="text">The best way to find yourself is to lose yourself in the service of others.... </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--End Services Section-->

<!--Welcome Section-->
<section class="welcome-section no-padd-top">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title">
            <h2>Welcome to Women at Heart Initiative</h2>
        </div>
        <div class="row clearfix">
            <!--Video Column-->
            <div class="video-column col-md-6 col-sm-12 col-xs-12">
                <!--Video Box-->
                <div class="video-box">
                    <figure class="image">
                        <img src="images/resource/video-img.png" alt="">
                    </figure>
                    <a href="#" class="lightbox-image overlay-box"><span class="flaticon-right-arrow-2"></span></a>
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="text">
                        @if ($about->count()>0)
                        @foreach ($about as $value)
                        {!! str_limit($value->content, 750) !!}
                        @endforeach
                        @else
                        <div style="color:#ff0000">No Post Yet!</div>
                        @endif
                    </div>
                    <a href="/about-us" class="theme-btn btn-style-three">Read More About Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Welcome Section-->

<!--Call To Action Two-->
<section class="call-to-action-two" style="background-image:url(images/background/3.jpg)">
    <div class="auto-container">
        <div class="inner-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-8 col-sm-12 col-xs-12">
                    <h2>Want to participate as a volunteer?</h2>
                </div>
                <!--Column-->
                <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                    <a href="/contact-us" class="theme-btn btn-style-two">Contact Now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action Two-->

<!--Causes Section-->
<section class="causes-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title">
            <h2>Our Causes</h2>
        </div>
        <div class="four-item-carousel owl-carousel owl-theme">
            @if ($cause->count()>0)
            @foreach ($cause as $value)
            <!--Causes Block-->
            <div class="causes-block">
                <div class="inner-box">
                    <div class="image">
                        <a href="/our-causes/{{ $value->slug }}"><img src="{{ asset('images/cause/'.$value->smallpic) }}" alt="" /></a>
                    </div>
                    <div class="lower-content">
                        <h3><a href="/our-causes/{{ $value->slug }}">{!! str_limit($value->title, 20) !!}</a></h3>
                        <div class="content">
                            <div class="text">{!! str_limit($value->content, 120) !!}</div>
                            <div class="overlay-box">
                                <div class="collect">We need to collect <span class="theme_color">{{ number_format($value->amount, 2) }}</span></div>
                            </div>
                        </div>
                        <div class="btns-box">
                            <a href="/your-donation" class="theme-btn btn-style-two">Donate</a>
                            <a href="/our-causes/{{ $value->slug }}" class="theme-btn btn-style-four">More Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div style="color:#ff0000">No Cause has been posted yet!</div>
            @endif

        </div>
    </div>
</section>
<!--End Cases Section-->

<!--Call To Action Two-->
<section class="call-to-action-two" style="background-image:url({{ asset('images/background/3.jpg') }})">
    <div class="auto-container">
        <div class="inner-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-8 col-sm-12 col-xs-12">
                    <h2>Donate now for a good cause.</h2>
                </div>
                <!--Column-->
                <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                    <a href="/your-donation" class="theme-btn btn-style-two">Donate</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action Two-->

<!--Events Section-->
<section class="events-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title">
            <h2>Upcoming Events</h2>
        </div>
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="row clearfix">
                    @if ($event->count()>0)
                    @foreach ($event as $value)
                    <!--Event Block-->
                    <div class="event-block col-md-6 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="{{ asset('images/event/'.$value->smallpic) }}" alt="" />
                                <a href="#" class="overlay-box">
                                    <div class="text">Donate Now</div>
                                </a>
                            </div>
                            <div class="lower-box">
                                <div class="post-info">{{ date('F d, Y', strtotime($value->event_date)) }} / <span class="theme_color">{{ $value->county->name }}</span></div>
                                <h3><a href="/our-events/{{ $value->slug }}">{!! str_limit($value->title, 25) !!}</a></h3>
                                <div class="text">{!! str_limit($value->content, 125) !!}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div style="color:#ff0000">No Event has been posted yet!</div>
                    @endif

                </div>
            </div>
            <!--Column-->
            <div class="column col-lg-4 col-md-6 col-sm-12 col-xs-12">
                @if ($event2->count()>0)
                @foreach ($event2 as $value)
                <!--Event Block Two-->
                <div class="event-block-two">
                    <div class="inner-box">
                        <div class="content">
                            <div class="date-box">{{ date('d', strtotime($value->event_date)) }} <span>{{ date('M', strtotime($value->event_date)) }}</span></div>
                            <h3><a href="/our-events/{{ $value->slug }}">Event: {!! str_limit($value->title, 15) !!}</a></h3>
                            <div class="post-info">{{ date('F d, Y', strtotime($value->event_date)) }} /  <span class="theme_color">{{ $value->county->name }}.</span></div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div style="color:#ff0000">No Event has been posted yet!</div>
                @endif
            </div>
        </div>
    </div>
</section>
<!--End Events Section-->

@endsection

@section('style')
<style>
.jumbotron {
    margin-bottom: 0px;
    background-image: url({{ asset('images/background/image-4.jpg') }});
    background-size: cover;
    background-repeat: no-repeat;
    height: 450px;
    color: white;
    text-align: center;
    text-shadow: black 0.3em 0.3em 0.3em;
}
</style>
@endsection