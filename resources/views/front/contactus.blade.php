@extends('front.layout.main')
@section('title', 'Contact')
@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title -->
            <div class="title-column col-md-6 col-sm-8 col-xs-12">
                <h1>Contact Us</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li class="active">Contact</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->

<!--Contact Info Section-->
<section class="info-contact-section">
    <div class="auto-container">
        <div class="row clearfix">
            
            <!--Info Column-->
            <div class="info-column col-md-4 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="icon-box">
                        <span class="icon flaticon-world"></span>
                    </div>
                    <h3>Address</h3>
                    <div class="text">Cecil Sharp House14 Tottenham <br> Elodret, Kenya.</div>
                </div>
            </div>
            
            <!--Info Column-->
            <div class="info-column col-md-4 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="icon-box">
                        <span class="icon flaticon-technology-1"></span>
                    </div>
                    <h3>Phone</h3>
                    <div class="text"> can call us on phone number. <br> (+254)-724-124-569</div>
                </div>
            </div>
            
            <!--Info Column-->
            <div class="info-column col-md-4 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="icon-box">
                        <span class="icon flaticon-symbol"></span>
                    </div>
                    <h3>Email</h3>
                    <div class="text">info@womenatheartinitiative.org <br> www.womenatheartinitiative.org</div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Contact Info Section-->

<!--Contact Form Section-->
<div class="contact-form-section">
    <div class="auto-container">
        <h2>Get in Touch</h2>
        <div class="text">if you want to get more info, ping us now.</div>
        @if (session('status'))
          <small style="color: green;"><strong>{{ session('status') }}</strong></small>
          @endif
        <!--Contact Form-->
        <div class="contact-form">
            {!! Form::open(['action'=>'PageController@messageus', 'method' =>'POST']) !!}
                <div class="row clearfix">
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        {!! Form::text('name', '', array('class' => 'form-control', 'placeholder'=>'Name', 'required')) !!}
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                <small style="color: red;">{{ $errors->first('name') }}</small>
                            </div>
                        @endif()
                    </div>
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        {!! Form::email('email', '', array('class' => 'form-control', 'placeholder'=>'Email', 'required')) !!}
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                <small style="color: red;">{{ $errors->first('email') }}</small>
                            </div>
                        @endif()
                    </div>
                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        {!! Form::text('title', '', array('class' => 'form-control', 'placeholder'=>'Subject', 'required')) !!}
                        @if($errors->has('title'))
                            <div class="invalid-feedback">
                                <small style="color: red;">{{ $errors->first('title') }}</small>
                            </div>
                        @endif()
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        {!! Form::textarea('content', '', array('class' => 'form-control', 'placeholder'=>'Message')) !!}
                        @if($errors->has('content'))
                            <div class="invalid-feedback">
                                <small style="color: red;">{{ $errors->first('content') }}</small>
                            </div>
                        @endif()
                    </div>
                    
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
                        <button type="submit" class="theme-btn btn-style-two">Send Now</button>
                    </div>
                    
                </div>
                
            </form>
        </div>
        
    </div>
</div>

@endsection
