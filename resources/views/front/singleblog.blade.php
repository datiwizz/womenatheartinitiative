@extends('front.layout.main')
@section('title', 'Blog Detail')
@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{ asset('images/background/banner-4.jpg') }});">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title -->
            <div class="title-column col-md-6 col-sm-8 col-xs-12">
                <h1>Blog Detail</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li class="active">Blog Detail</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <div class="blog-single">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{ asset('images/blog/'.$blog->bigpic) }}" alt="" />
                            <div class="post-date">{{ date('d', strtotime($blog->created_at)) }} <span>{{ date('M', strtotime($blog->created_at)) }}</span></div>
                        </div>
                        <h3>{!! $blog->title !!}</h3>
                        <ul class="post-meta">
                            <li><span class="icon flaticon-user-1"></span>{{ $blog->user->name }}</li>
                        </ul>
                        <div class="text">
                            {!! $blog->content !!}
                        </div>

                        <!--post-share-options-->
                        <div class="post-share-options clearfix">
                            <div class="pull-left tags"><span>Category:</span><a href="#">{{ $blog->category->name }}</a></div>
                            <div class="pull-right">
                                <div class="social-buttons">
                                    <strong>Share:</strong>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}"
                                    target="_blank">
                                    <i class="fa fa-facebook-official fa-lg"></i>
                                        </a>
                                        <a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}"
                                        target="_blank">
                                        <i class="fa fa-twitter-square fa-lg"></i>
                                    </a>
                                    <a href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}"
                                    target="_blank">
                                    <i class="fa fa-google-plus-square fa-lg"></i>
                                </a>
                            </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Comment Form -->

                <!--End Comment Form -->

            </div>

            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                @include('front.layout.sidebar')
            </div>

        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var popupSize = {
        width: 400,
        height: 400
    };
    $(document).on('click', '.social-buttons > a', function(e){

        var
        verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
        horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);
        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }
    });
</script>
@endsection