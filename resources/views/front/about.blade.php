@extends('front.layout.main')
@section('title', 'About')
@section('content')

  <!--Page Title-->
 <section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
   <div class="auto-container">
       <div class="row clearfix">
           <!--Title -->
           <div class="title-column col-md-6 col-sm-8 col-xs-12">
               <h1>About Us</h1>
             </div>
             <!--Bread Crumb -->
             <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                 <ul class="bread-crumb clearfix">
                     <li><a href="{{ url('') }}">Home</a></li>
                     <li class="active">About Us</li>
                 </ul>
             </div>
         </div>
     </div>
 </section>
 <!--End Page Title-->

 <!--Point Section-->
 <section class="point-section">
   <div class="auto-container">
       <div class="inner-container">
             <div class="sec-title">
                 <h2>About Women at Heart Initiative</h2>
             </div>
             <div class="text">
                 @if ($about->count()>0)
              @foreach ($about as $value)
                {!! $value->content !!}
              @endforeach
            @else
              <div style="color:#ff0000">No Post Yet!</div>
            @endif
             </div>
         </div>
     </div>
 </section>
 <!--End Point Section-->

 <!--Call To Action Two-->
 <section class="call-to-action-two" style="background-image:url(images/background/3.jpg)">
     <div class="auto-container">
         <div class="inner-container">
             <div class="row clearfix">
                 <!--Column-->
                 <div class="column col-md-8 col-sm-12 col-xs-12">
                     <h2>Want to participate as a volunteer?</h2>
                 </div>
                 <!--Column-->
                 <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                     <a href="{{ url('contact-us') }}" class="theme-btn btn-style-two">Contact Now</a>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!--End Call To Action Two-->

 <!--Default Section-->
 <section class="default-section">
   <div class="auto-container">
       <div class="row clearfix">

           <!--Accordian Column-->
             <div class="accordian-column col-md-12 col-sm-12 col-xs-12">
               <div class="sec-title">
                   <h2>Why Choos Us</h2>
                 </div>

                 <!--Accordion Box-->
                 <ul class="accordion-box">

                     <!--Block-->
                     <li class="accordion block active-block">
                         <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-caret-right"></span></div>Our Mission</div>
                         <div class="acc-content current">
                             <div class="content"><p>To empower women and vulnerable groups through Education, Health and Poverty programs.</p></div>
                         </div>
                     </li>

                     <!--Block-->
                     <li class="accordion block">
                         <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-caret-right"></span> </div>Our Vision</div>
                         <div class="acc-content">
                             <div class="content"><p>A country in which women and youth attains the right to education, health, and poverty free society.</p></div>
                         </div>
                     </li>

       </ul>

             </div>

         </div>
     </div>
 </section>
 <!--End Default Section-->


@endsection
