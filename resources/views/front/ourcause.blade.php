@extends('front.layout.main')
@section('title', 'Causes')
@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{ asset('images/background/banner-4.jpg') }});">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title -->
            <div class="title-column col-md-6 col-sm-8 col-xs-12">
                <h1>Our Cause</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li class="active">Causes list</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <div class="causes-list">
                    @if ($cause->count()>0)
                    @foreach ($cause as $value)
                    <!--Causes Block-->
                    <div class="causes-block-two">
                        <div class="inner-box">
                            <div class="row clearfix">
                                <div class="image-column col-md-4 col-sm-4 col-xs-12">
                                    <div class="image">
                                        <img src="{{ asset('images/cause/'.$value->bigpic) }}" alt="" />
                                        <a href="/our-causes/{{ $value->slug }}" class="overlay-box"><span class="icon flaticon-unlink"></span></a>
                                    </div>
                                </div>
                                <div class="content-column col-md-8 col-sm-8 col-xs-12">
                                    <div class="inner-column">
                                        <h3><a href="/our-causes/{{ $value->slug }}">{!! str_limit($value->title, 30) !!}</a></h3>
                                        <div class="collect">We need to collect <span class="theme_color">{{ number_format($value->amount, 2) }}</span></div>
                                        <div class="text">{!! str_limit($value->content, 120) !!} </div>
                                        <!--Skills-->
                                        <div class="skills style-two">
                                            <div class="clearfix donate-percent">
                                                <div class="pull-right number">We have raised <strong>{{ number_format($value->raised, 2) }}</strong></div>
                                            </div>
                                        </div>
                                        <div class="btns-box">
                                            <a href="/your-donation" class="theme-btn btn-style-two">Donate</a>
                                            <a href="/our-causes/{{ $value->slug }}" class="theme-btn btn-style-four">More Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div style="color:#ff0000">No Cause has been posted yet!</div>
                    @endif

                    <!--Styled Pagination-->
                    <div class="styled-pagination">
                        {{ $cause->links() }}
                    </div>
                    <!--End Styled Pagination-->

                </div>
            </div>

            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                @include('front.layout.sidebar')
            </div>

        </div>
    </div>
</div>

@endsection
