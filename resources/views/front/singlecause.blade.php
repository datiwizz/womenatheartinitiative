@extends('front.layout.main')
@section('title', 'Cause Detail')
@section('content')

  <!--Page Title-->
 <section class="page-title" style="background-image:url({{ asset('images/background/banner-4.jpg') }});">
   <div class="auto-container">
       <div class="row clearfix">
           <!--Title -->
           <div class="title-column col-md-6 col-sm-8 col-xs-12">
               <h1>Cause Detail</h1>
             </div>
             <!--Bread Crumb -->
             <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                 <ul class="bread-crumb clearfix">
                     <li><a href="{{ url('') }}">Home</a></li>
                     <li class="active">Cause Detail</li>
                 </ul>
             </div>
         </div>
     </div>
 </section>
 <!--End Page Title-->

 <!--Sidebar Page Container-->
 <div class="sidebar-page-container">
   <div class="auto-container">
       <div class="row clearfix">

             <!--Content Side / Causes Single-->
             <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
       <div class="causes-single">
                   <div class="inner-box">
                       <div class="upper-box">
                         <h2>{!! $data->title !!}</h2>
                         </div>
                         <div class="image">
                           <img src="{{ asset('images/cause/'.$data->bigpic) }}" alt="" />
                         </div>
                         <div class="clearfix">
                           <div class="pull-left">
                               <h3>Cause Description</h3>
                             </div>
                             <div class="pull-right collect-box">
                               <div class="collection">We need to collect <span class="theme_color">8,600,00</span></div>
                             </div>
                         </div>
                         <div class="text">
                           {!! $data->content !!}
                         </div>
                         <div class="post-share-options clearfix">
                                <div class="pull-left">
                                    <a href="/your-donation" class="theme-btn btn-style-two">Donate</a>
                                </div>
                            </div>
                     </div>
                 </div>

                 <!-- Comment Form -->
                 <div class="comment-form">
                     <div class="group-title"><h2>Leave Your Comments</h2></div>
                     <!--Comment Form-->
                     <form method="post" action="#">
                         <div class="row clearfix">
                             <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                 <input type="text" name="username" placeholder="Name" required>
                             </div>

                             <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                 <input type="email" name="email" placeholder="Email" required>
                             </div>

                             <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                 <input type="text" name="subject" placeholder="Subject" required>
                             </div>

                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                 <textarea name="message" placeholder="Your Comments"></textarea>
                             </div>

                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                 <button class="theme-btn btn-style-two" type="submit" name="submit-form">Post Comment</button>
                             </div>

                         </div>
                     </form>

                 </div>
                 <!--End Comment Form -->

             </div>

             <!--Sidebar Side-->
             <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
              @include('front.layout.sidebar')
             </div>

         </div>
     </div>
 </div>


@endsection
