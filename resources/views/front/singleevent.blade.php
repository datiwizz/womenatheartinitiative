@extends('front.layout.main')
@section('title', 'Event Detail')
@section('content')

  <!--Page Title-->
   <section class="page-title" style="background-image:url({{ asset('images/background/banner-4.jpg') }});">
     <div class="auto-container">
         <div class="row clearfix">
             <!--Title -->
             <div class="title-column col-md-6 col-sm-8 col-xs-12">
                 <h1>Event Detail</h1>
               </div>
               <!--Bread Crumb -->
               <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                   <ul class="bread-crumb clearfix">
                       <li><a href="{{ url('') }}">Home</a></li>
                       <li class="active">Event Detail</li>
                   </ul>
               </div>
           </div>
       </div>
   </section>
   <!--End Page Title-->

   <!--Sidebar Page Container-->
   <div class="sidebar-page-container">
     <div class="auto-container">
         <div class="row clearfix">

               <!--Content Side-->
               <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
         <div class="events-single">
                     <div class="inner-box">
                         <div class="image">
                             <img src="{{ asset('images/event/'.$event->bigpic) }}" alt="" />
                           </div>
                           <div class="lower-content">
                             <div class="post-info">{{ date('F d, Y', strtotime($event->event_date)) }} / <span class="theme_color">{{ $event->county->name }}.</span></div>
                               <h3>{!! $event->title !!}</h3>
                               <div class="text">
                                 {!! $event->content !!}
                               </div>
                           </div>
                       </div>
                   </div>
               </div>

               <!--Sidebar Side-->
               <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">

                @include('front.layout.sidebar')

               </div>

           </div>
       </div>
   </div>

@endsection
