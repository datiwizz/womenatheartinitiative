@extends('front.layout.main')
@section('title', 'Our Constitution')
@section('content')

  <!--Page Title-->
 <section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
   <div class="auto-container">
       <div class="row clearfix">
           <!--Title -->
           <div class="title-column col-md-6 col-sm-8 col-xs-12">
               <h1>Our Constitution</h1>
             </div>
             <!--Bread Crumb -->
             <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                 <ul class="bread-crumb clearfix">
                     <li><a href="{{ url('') }}">Home</a></li>
                     <li class="active">Our Constitution</li>
                 </ul>
             </div>
         </div>
     </div>
 </section>
 <!--End Page Title-->

 <!--Point Section-->
 <section class="point-section">
   <div class="auto-container">
       <div class="inner-container">
             <div class="sec-title">
                 <h2>Women at Heart Initiative - Constitution</h2>
             </div>
             <div class="text">
                 @if ($constitution->count()>0)
              @foreach ($constitution as $value)
                {!! $value->content !!}
              @endforeach
            @else
              <div style="color:#ff0000">No Constitution Post Yet!</div>
            @endif
             </div>
         </div>
     </div>
 </section>
 <!--End Point Section-->

@endsection
