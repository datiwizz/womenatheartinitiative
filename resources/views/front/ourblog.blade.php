@extends('front.layout.main')
@section('title', 'Blog')
@section('content')

  <!--Page Title-->
 <section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
   <div class="auto-container">
       <div class="row clearfix">
           <!--Title -->
           <div class="title-column col-md-6 col-sm-8 col-xs-12">
               <h1>Blog Default</h1>
             </div>
             <!--Bread Crumb -->
             <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                 <ul class="bread-crumb clearfix">
                     <li><a href="{{ url('') }}">Home</a></li>
                     <li class="active">Blog Default</li>
                 </ul>
             </div>
         </div>
     </div>
 </section>
 <!--End Page Title-->

 <!--Sidebar Page Container-->
 <div class="sidebar-page-container">
   <div class="auto-container">
       <div class="row clearfix">

             <!--Content Side-->
             <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">

                 @if ($blog->count()>0)
        @foreach ($blog as $value)

       <div class="blog-single cp-blog-default">
                   <div class="inner-box">
                       <div class="image">
                           <a href="our-blog/{{ $value->slug }}"><img src="{{ asset('images/blog/'.$value->smallpic) }}" alt="" /></a>
                             <div class="post-date">{{ date('d', strtotime($value->created_at)) }} <span>{{ date('M', strtotime($value->created_at)) }}</span></div>
                         </div>
                         <h3><a href="our-blog/{{ $value->slug }}">{!! $value->title !!}</a></h3>
                         <ul class="post-meta">
                           <li><span class="icon flaticon-user-1"></span>{{ $value->user->name }}</li>
                         </ul>
                         <div class="text">
                           {!! str_limit($value->content, 225) !!}
                         </div>

                     </div>
                 </div>
                 @endforeach
      @else
        <div style="color:#ff0000">No Blog has been posted yet!</div>
      @endif

                  <!--Styled Pagination-->
                 <div class="styled-pagination">
                     {{ $blog->links() }}
                 </div>
                 <!--End Styled Pagination-->

             </div>

             <!--Sidebar Side-->
             <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
               @include('front.layout.sidebar')
             </div>

         </div>
     </div>
 </div>

@endsection
