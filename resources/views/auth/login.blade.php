<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Login | Women at Heart Initiative</title>
<!-- Stylesheets -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">

<!-- Preloader -->
<div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header header-style-two">

        <!-- Header Top Two-->
        <div class="header-top-two">
            <div class="auto-container">
                <div class="clearfix">

                    <!--Top Left-->
                    <div class="top-left">
                        <!--social-icon-->
                        <div class="social-icon">
                            <ul class="clearfix">
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Header Top End -->

        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                <div class="clearfix">

                    <div class="pull-left logo-outer">
                        <div class="logo"><a href="{{ url('') }}"><img src="{{ asset('images/logo.png') }}" alt="" title=""></a></div>
                    </div>

                    <div class="pull-right upper-right clearfix">

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-technology-1"></span><strong>Phone.</strong></li>
                                <li>(+254) 724-124-569</li>
                            </ul>
                        </div>

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-symbol"></span><strong>Email</strong></li>
                                <li>info@womenatheartinitiative.org</li>
                            </ul>
                        </div>

                        <!--Info Box-->
                        <div class="upper-column info-box">
                            <ul>
                                <li><span class="icon flaticon-location-pin"></span><strong>Location</strong></li>
                                <li>Eldoret, Kenya.</li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!--Header Lower-->
        <div class="header-lower">
            <div class="auto-container">
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li {{ (Request::is('/') ? 'class=current' : '') }}><a href="{{ url('') }}">Home</a></li>
                                <li {{ (Request::is('*about-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('about-us') }}">About Us</a>
                                </li>
                                <li {{ (Request::is('*our-causes') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-causes') }}">Causes</a>
                                </li>
                                <li {{ (Request::is('*our-events') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-events') }}">Events</a>
                                </li>
                                <li {{ (Request::is('*our-blog') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-blog') }}">Blog</a>
                                </li>
                                <li {{ (Request::is('*contact-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('contact-us') }}">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    @if (Route::has('login'))
                        <div class="outer-box">
                            @auth
                                <a href="{{ url('/dashboard') }}" class="theme-btn btn-style-one">Dashboard</a>
                            @else
                                <a href="{{ route('login') }}" class="theme-btn btn-style-one">Login</a>
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!--End Header Lower-->

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="{{ url('') }}" class="img-responsive" title="Tali"><img src="{{ asset('images/logo-small.png') }}" alt="" title=""></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li {{ (Request::is('/') ? 'class=current' : '') }}><a href="{{ url('') }}">Home</a></li>
                                <li {{ (Request::is('*about-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('about-us') }}">About Us</a>
                                </li>
                                <li {{ (Request::is('*our-causes') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-causes') }}">Causes</a>
                                </li>
                                <li {{ (Request::is('*our-events') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-events') }}">Events</a>
                                </li>
                                <li {{ (Request::is('*our-blog') ? 'class=current' : '') }}>
                                    <a href="{{ url('our-blog') }}">Blog</a>
                                </li>
                                <li {{ (Request::is('*contact-us') ? 'class=current' : '') }}>
                                    <a href="{{ url('contact-us') }}">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>

            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->

<!--Page Title-->
<section class="page-title" style="background-image:url(images/background/banner-4.jpg);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title -->
            <div class="title-column col-md-6 col-sm-8 col-xs-12">
                <h1>Login</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li class="active">Login</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End Page Title-->

<section class="donate-now">
    	<div class="auto-container">
        	<div class="default-form">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                    <div class="row clearfix">
                        
                        <!--Left Column-->
                        <div class="col-md-6 col-md-offset-4">
                            <div class="default-title"><h3>Login to your account</h3></div>
                            <div class="row clearfix">
                            
                               
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">Email *</div>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">Password *</div>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>
                                {{-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                                </div> --}}
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                   <button type="submit" class="theme-btn btn-style-one">Login</button>
                                   {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a> --}}
                                </div>
                                
                            </div>
                            
                        </div>
                    
                         
                    </div>
                </form>
                
            </div>
        </div>
    </section>

<!--Main Footer-->
<footer class="main-footer">
   <!--Footer Bottom-->
   <div class="footer-bottom">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-8 col-xs-12">
                    <div class="copyright">Copyright &copy; {!! date('Y') !!} Women at Heart Initiative. All rights reserved</div>
                </div>
                <div class="social-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="social-icon-one style-two">
                        <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
   </div>
</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


<script src="{{ asset('js/jquery.js') }}"></script>
<!--Revolution Slider-->
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{ asset('js/main-slider-script.js')}}"></script>

<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/isotope.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js')}}"></script>
<script src="{{ asset('js/jquery.fancybox-media.js')}}"></script>
<script src="{{ asset('js/owl.js')}}"></script>
<script src="{{ asset('js/wow.js')}}"></script>
<script src="{{ asset('js/appear.js')}}"></script>
<script src="{{ asset('js/script.js')}}"></script>

</body>

</html>
