@extends('back.layout.main')
@section('title', 'New County')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['route' => 'county.store', 'method' => 'POST', 'class'=>'bs-example form-horizontal']) !!}

@include('back.county._partials.form', ['btntext'=>'Create'])

</form>
</div>
</section>

@endsection
