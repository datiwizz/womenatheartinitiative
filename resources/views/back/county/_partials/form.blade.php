<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Name</label>
  <div class="col-lg-10">
    {!! Form::text('name', $data->name, array('class' => 'form-control', 'placeholder'=>'County Name')) !!}
    @if($errors->has('name'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('name') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $data->status, ['placeholder' => 'Pick a Status...', 'class'=>'form-control']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Description</label>
  <div class="col-lg-10">
    {!! Form::textarea('content', $data->content, array('class' => 'form-control')) !!}
    @if($errors->has('content'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('content') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('county.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>
