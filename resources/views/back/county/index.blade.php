@extends('back.layout.main')
@section('title', 'County')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <header class="panel-heading"><a href="{{route('county.create')}}" class="btn btn-s-md btn-info btn-sm">New County</a></header>
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th>#</th>
        <th>County</th>
        <th>Status</th>
        <th width="70"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @if (!empty($data) && $data->count())
          @foreach ($data as $key => $value)
            <tr>
              <td> {{++$key}} </td>
              <td> {!! $value->name !!} </td>
              <td style="font-weight:bold;">
								@if ($value->status === 1)
								<span style="color: #090;">Active</span>
              @elseif ($value->status === 0)
								<span style="color: #f9243f;">Inactive</span>
								@endif
							</td>
              <td>  <a href="{{route('county.edit', $value->slug)}}"> <i class="fa fa-edit"></i> </a> </td>
            </tr>
          @endforeach
        @else
          <tr class="table-danger" align="center">
            <td colspan="4" style="color:#ff0000">
              <div class="alert alert-danger">
                <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                <a href="{{route('county.create')}}" class="alert-link">Add a County</a>.
              </div>
            </td>
          </tr>
        @endif
      </tr>
    </tbody>
  </table>
</section>

@endsection
