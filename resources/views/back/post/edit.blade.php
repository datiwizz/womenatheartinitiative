@extends('back.layout.main')
@section('title', 'Edit Post')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">
  {!! Form::open(['method'=>'DELETE', 'route'=>['post.destroy',$data->id]]) !!}
  <button data-toggle="tooltip" data-placement="top" title="Delete" type="submit" class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this post?');">Delete</button>
  {!! Form::close() !!}
  Update form</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['post.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal', 'files'=>true]) !!}

@can('isAdmin')
  @include('back.post._partials.form', ['btntext' => 'Update'])
@elsecan('isAuthor')
  <input type="hidden" class="form-control" name="status" value="0">
  @include('back.post._partials.form', ['btntext' => 'Update'])
@endcan

</form>
</div>
</section>

@endsection
