@extends('back.layout.main')
@section('title', 'Post')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <header class="panel-heading"><a href="{{route('post.create')}}" class="btn btn-s-md btn-info btn-sm">New Post</a></header>
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th>#</th>
        <th>Title</th>
        <th>Creator</th>
        <th>County</th>
        <th>Status</th>
        <th width="70"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @if (!empty($data) && $data->count())
          @foreach ($data as $key => $value)
            <tr>
              <td> {{ $key + $data->firstItem() }} </td>
              @can('isAdmin')
                <td> <a href="/post/{{ $value->slug }}">{!! $value->title !!}</a> </td>
              @elsecan('isAuthor')
                @if($value->status == 0)
              <td> <a href="/post/{{ $value->slug }}">{!! $value->title !!}</a> </td>
              @else
              <td> {!! $value->title !!} </td>
              @endif
              @endcan
              <td> {!! $value->user->name !!} </td>
              <td> {!! $value->county->name !!} </td>
              <td style="font-weight:bold;">
								@if ($value->status === 1)
								<span style="color: #090;">Active</span>
              @elseif ($value->status === 0)
								<span style="color: #f9243f;">Inactive</span>
								@endif
              </td>
              @can('isAdmin')
                <td>  <a href="{{route('post.edit', $value->slug)}}"> <i class="fa fa-edit"></i> </a> </td>
              @elsecan('isAuthor')
                 @if($value->status == 0)         
                <td>  <a href="{{route('post.edit', $value->slug)}}"> <i class="fa fa-edit"></i> </a> </td>         
                @else
                  <td> <i class="fa fa-ban"></i> </td>      
                @endif
              @endcan
            </tr>
          @endforeach
        @else
          <tr class="table-danger" align="center">
            <td colspan="6" style="color:#ff0000">
              <div class="alert alert-danger">
                <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                <a href="{{route('post.create')}}" class="alert-link">Add a Post</a>.
              </div>
            </td>
          </tr>
        @endif
      </tr>
    </tbody>
  </table>
</section>
<div class="text-right text-center-xs">
{!! $data->render() !!}
</div>
@endsection

