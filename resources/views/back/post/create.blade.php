@extends('back.layout.main')
@section('title', 'Create Post')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['action'=>'PostController@store', 'method' =>'POST', 'class'=>'bs-example form-horizontal', 'files'=>true]) !!}

@can('isAdmin')
  @include('back.post._partials.form', ['btntext' => 'Create'])
@elsecan('isAuthor')
  <input type="hidden" class="form-control" name="status" value="0">
  @include('back.post._partials.form', ['btntext' => 'Create'])
@endcan

</form>
</div>
</section>

@endsection
