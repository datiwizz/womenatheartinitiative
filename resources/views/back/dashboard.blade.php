@extends('back.layout.main')
@section('title', 'Dashboard')
@section('content')

<section class="panel panel-default">
  <div class="row m-l-none m-r-none bg-light lter">
    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
      <span class="fa-stack fa-2x pull-left m-r-sm"> 
        <i class="fa fa-circle fa-stack-2x text-info"></i> 
        <i class="fa fa-paste fa-stack-1x text-white"></i> 
      </span> 
      <a class="clear" href="{{ url('/post') }}"> 
        <span class="h3 block m-t-xs"><strong>{{ $postsum->count('id') }}</strong></span> 
        <small class="text-muted text-uc">Posts</small> 
      </a>
    </div>
    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
      <span class="fa-stack fa-2x pull-left m-r-sm"> 
        <i class="fa fa-circle fa-stack-2x text-warning"></i> 
        <i class="fa fa-map-marker fa-stack-1x text-white"></i> 
      </span> 
      <a class="clear" href="{{ url('/county') }}"> 
        <span class="h3 block m-t-xs"><strong>{{ $countysum->count('id') }}</strong></span> 
        <small class="text-muted text-uc">Counties</small> 
      </a>
    </div>
    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
      <span class="fa-stack fa-2x pull-left m-r-sm"> 
        <i class="fa fa-circle fa-stack-2x text-danger"></i> 
        <i class="fa fa-road fa-stack-1x text-white"></i> 
      </span> 
      <a class="clear" href="{{ url('/cause') }}"> 
        <span class="h3 block m-t-xs"><strong>{{ $causesum->count('id') }}</strong></span> 
        <small class="text-muted text-uc">Causes</small> 
      </a>
    </div>
    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
      <span class="fa-stack fa-2x pull-left m-r-sm"> 
        <i class="fa fa-circle fa-stack-2x text-primary"></i> 
        <i class="fa fa-male fa-stack-1x text-white"></i> 
      </span> 
      <a class="clear" href="{{ url('/event') }}"> 
        <span class="h3 block m-t-xs"><strong>{{ $eventsum->count('id') }}</strong></span> 
        <small class="text-muted text-uc">Events</small> 
      </a>
    </div>
  </div>
</section>

<div class="row">
  <div class="col-sm-6 portlet ui-sortable">
    <section class="panel panel-default portlet-item">
      <header class="panel-heading">
        <ul class="nav nav-pills pull-right">
          <li> <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a> </li>
        </ul> Event(s) <span class="badge bg-info">{{ $eventsum->count('id') }}</span> </header>
        <section class="panel-body">
          @if ($event->count()>0)
          @foreach ($event as $value)
          <article class="media">
            <div class="pull-left"> 
              <span class="pull-left thumb-sm"><img src="{{ asset('images/event/'.$value->smallpic) }}" class="img-circle"></span>
            </div>
            <div class="media-body"> <a href="/event/{{ $value->slug }}" class="h4">{!! str_limit($value->title, 50) !!}</a> <small class="block m-t-xs">
              {!! str_limit($value->content, 150) !!}
            </small> 
            <em class="text-xs">Event date <span class="text-danger">{{ date('F d, Y', strtotime($value->event_data)) }}</span></em>                          
          </div>
        </article>
        <div class="line pull-in"></div>
        @endforeach
        @else
        <div style="color:#ff0000">No Post has been posted yet!</div>
        @endif
      </section>
    </section>
  </div>
  <div class="col-sm-6 portlet ui-sortable">
    <section class="panel panel-default portlet-item">
      <header class="panel-heading"> <span class="label bg-dark">{{ $causesum->count('id') }}</span> Cause(s) </header>
      <section class="panel-body">

        @if ($cause->count()>0)
        @foreach ($cause as $value)
        <article class="media"> <span class="pull-left thumb-sm"><img src="{{ asset('images/cause/'.$value->smallpic) }}" class="img-circle"></span>
          <div class="media-body">
            <div class="pull-right media-xs text-center text-muted"> <strong class="h4">{{ date('d', strtotime($value->created_at)) }}</strong><br> <small class="label bg-light">{{ date('M', strtotime($value->created_at)) }}</small> </div> <a href="/cause/{{ $value->slug }}" class="h4">{!! str_limit($value->title, 50) !!}</a> <small class="block"><a href="#" class="">{{ $value->user->name }}</a> <span class="label label-info">{{ $value->county->name }}</span></small> <small class="block m-t-sm">{!! str_limit($value->content, 150) !!}</small> </div>
          </article>
          <div class="line pull-in"></div>
          @endforeach
          @else
          <div style="color:#ff0000">No Cause has been posted yet!</div>
          @endif

        </section>
      </section>
    </div>
  </div>

  @endsection
