@extends('back.layout.main')
@section('title', 'Edit Profile')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
<header class="panel-heading font-bold">
  @can('isAdmin')
  {!! Form::open(['method'=>'DELETE', 'route'=>['user.destroy',$data->id]]) !!}
  <button data-toggle="tooltip" data-placement="top" title="Delete" type="submit" class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this User and Related Posts?');">Delete</button>
  {!! Form::close() !!}
  @endcan
  Update form
</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['user.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal']) !!}

@can('isAdmin')
    @include('back.user._partials.form', ['btntext'=>'Update', 'disable' => false])
@elsecan('isAuthor')
    <input type="hidden" class="form-control" name="role" value="{{$data->role}}">
    <input type="hidden" class="form-control" name="status" value="{{$data->status}}">
    @include('back.user._partials.form', ['btntext'=>'Update', 'disable' => true])
@endcan

</form>
</div>
</section>

@endsection
