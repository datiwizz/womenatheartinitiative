@extends('back.layout.main')
@section('title', 'Create User')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['route' => 'user.store', 'method' => 'POST', 'class'=>'bs-example form-horizontal']) !!}

@include('back.user._partials.form', ['btntext'=>'Create', 'disable' => false])

</form>
</div>
</section>

@endsection
