@extends('back.layout.main')
@section('title', 'Users')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <header class="panel-heading"><a href="{{route('user.create')}}" class="btn btn-s-md btn-info btn-sm">New User</a></header>
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Role</th>
        <th>Status</th>
        @if(Auth::user()->role == 1)
        <th width="70"></th>
        @endif
      </tr>
    </thead>
    <tbody>
      <tr>
        @if (!empty($data) && $data->count())
          @foreach ($data as $key => $value)
            <tr>
              <td> {{ $key + $data->firstItem() }} </td>
              <td> {!! $value->name !!} </td>
              <td> {!! $value->phone !!} </td>
              <td style="font-weight:bold;">
                @if ($value->role === 1)
                <span class="text-danger">Admin</span>
                @elseif ($value->role === 0)
                <span class="text-success">Author</span>
                @endif
              </td>
              <td style="font-weight:bold;">
                @if ($value->status === 1)
                <span style="color: #090;">Active</span>
                @elseif ($value->status === 0)
                <span style="color: #f9243f;">Inactive</span>
                @endif
              </td>
              @if(Auth::user()->role == 1)
                  <td>  <a href="{{route('user.edit', $value->slug)}}"> <i class="fa fa-edit"></i> </a> </td>
              @endif
            </tr>
          @endforeach
        @else
          <tr class="table-danger" align="center">
            <td colspan="6" style="color:#ff0000">
              <div class="alert alert-danger">
                <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                <a href="{{route('user.create')}}" class="alert-link">Add some Users</a>.
              </div>
            </td>
          </tr>
        @endif
      </tr>
    </tbody>
  </table>
</section>
<div class="text-right text-center-xs">
{!! $data->render() !!}
</div>
@endsection
