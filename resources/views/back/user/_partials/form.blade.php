<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Name</label>
  <div class="col-lg-10">
    {!! Form::text('name', $data->name, array('class' => 'form-control', 'placeholder'=>'Full Name')) !!}
    @if($errors->has('name'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('name') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Email</label>
  <div class="col-lg-10">
    {!! Form::email('email', $data->email, array('class' => 'form-control', 'placeholder'=>'Email Address')) !!}
    @if($errors->has('email'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('email') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Phone</label>
  <div class="col-lg-10">
    {!! Form::text('phone', $data->phone, array('class' => 'form-control', 'placeholder'=>'Phone Number')) !!}
    @if($errors->has('phone'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('phone') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('idno') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">ID No.</label>
  <div class="col-lg-10">
    {!! Form::text('idno', $data->idno, array('class' => 'form-control', 'placeholder'=>'ID Number')) !!}
    @if($errors->has('idno'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('idno') }}</small>
      </span>
    @endif()
  </div>
</div>
@if ($disable)
<div class="form-group {{ $errors->has('role') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Role</label>
  <div class="col-lg-10">
    {!! Form::select('role', ['0' => 'Author', '1' => 'Admin'], $data->role, ['placeholder' => 'Pick a Role...', 'class'=>'form-control', 'disabled' => 'disabled']); !!}
    @if($errors->has('role'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('role') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $data->status, ['placeholder' => 'Pick a Status...', 'class'=>'form-control', 'disabled' => 'disabled']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div>
@else
<div class="form-group {{ $errors->has('role') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Role</label>
  <div class="col-lg-10">
    {!! Form::select('role', ['0' => 'Author', '1' => 'Admin'], $data->role, ['placeholder' => 'Pick a Role...', 'class'=>'form-control']); !!}
    @if($errors->has('role'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('role') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $data->status, ['placeholder' => 'Pick a Status...', 'class'=>'form-control']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div> 
@endif
<div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Password.</label>
  <div class="col-lg-10">
    {!! Form::password('password', ['class' => 'form-control']) !!}
    @if($errors->has('password'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('password') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    @can('isAdmin')
  <a href="{!! route('user.index') !!}" class="btn btn-sm btn-default">Back</a>
  @endcan
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>
