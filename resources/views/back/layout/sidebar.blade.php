<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
<section class="vbox">
<section class="w-f scrollable">
<div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
<!-- nav -->
<nav class="nav-primary hidden-xs">
<ul class="nav">
@can('isAdmin')
<li {{ Request::is('dashboard*') ? 'class=active' : null }}>
  <a href="{{url('/dashboard')}}" {{ Request::is('dashboard*') ? 'class=active' : null }}>
    <i class="fa fa-dashboard icon"> <b class="bg-danger"></b> </i> <span>Dashboard</span>
  </a>
</li>
<li {{ Request::is('county*') ? 'class=active' : null }}>
  <a href="{{url('/county')}}" {{ Request::is('county*') ? 'class=active' : null }}>
    <i class="fa fa-map-marker icon"> <b class="bg-success"></b> </i> <span>County</span>
  </a>
</li>
<li {{ Request::is('category*') ? 'class=active' : null }}>
  <a href="{{url('/category')}}" {{ Request::is('category*') ? 'class=active' : null }}>
    <i class="fa fa-tags icon"> <b class="bg-warning"></b> </i> <span>Category</span>
  </a>
</li>
<li {{ Request::is('about*') ? 'class=active' : null }}>
  <a href="{{url('/about')}}" {{ Request::is('about*') ? 'class=active' : null }}>
    <i class="fa fa-adn icon"> <b class="bg-primary"></b> </i> <span>About</span>
  </a>
</li>
<li {{ Request::is('constitution*') ? 'class=active' : null }}>
  <a href="{{url('/constitution')}}" {{ Request::is('constitution*') ? 'class=active' : null }}>
    <i class="fa fa-file icon"> <b class="bg-info"></b> </i> <span>Constitution</span>
  </a>
</li>
@endcan

<li {{ Request::is('post*') ? 'class=active' : null }}>
  <a href="{{url('/post')}}" {{ Request::is('post*') ? 'class=active' : null }}>
    <i class="fa fa-paste icon"> <b class="bg-danger"></b> </i> <span>Post</span>
  </a>
</li>

@can('isAdmin')
<li {{ Request::is('cause*') ? 'class=active' : null }}>
  <a href="{{url('/cause')}}" {{ Request::is('cause*') ? 'class=active' : null }}>
    <i class="fa fa-road icon"> <b class="bg-success"></b> </i> <span>Cause</span>
  </a>
</li>
<li {{ Request::is('event*') ? 'class=active' : null }}>
  <a href="{{url('/event')}}" {{ Request::is('event*') ? 'class=active' : null }}>
    <i class="fa fa-trophy icon"> <b class="bg-warning"></b> </i> <span>Event</span>
  </a>
</li>
<li {{ Request::is('user*') ? 'class=active' : null }}>
  <a href="{{url('/user')}}" {{ Request::is('user*') ? 'class=active' : null }}>
    <i class="fa fa-group icon"> <b class="bg-primary"></b> </i> <span>Users</span>
  </a>
</li>
<li {{ Request::is('newsletter*') ? 'class=active' : null }}>
  <a href="{{url('/newsletter')}}" {{ Request::is('newsletter*') ? 'class=active' : null }}>
    <i class="fa fa-folder-o icon"> <b class="bg-info"></b> </i> <span>Subscribers</span>
  </a>
</li>
<li {{ Request::is('contact*') ? 'class=active' : null }}>
  <a href="{{url('/contact')}}" {{ Request::is('contact*') ? 'class=active' : null }}>
    <i class="fa fa-envelope-o icon"> <b class="bg-success"></b> </i> <span>Message</span>
  </a>
</li>
@endcan
</ul>
</nav>
<!-- / nav -->
</div>
</section>
<footer class="footer lt hidden-xs b-t b-dark">
<a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
<div class="btn-group hidden-nav-xs">
<button type="button" title="Logout" class="btn btn-icon btn-sm btn-dark"><i class="fa fa-power-off"></i></button>
</div>
</footer>
</section>
</aside>
