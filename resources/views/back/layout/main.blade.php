@include('back.layout.header')

@include('back.layout.topbar')

<section>
<section class="hbox stretch">
<!-- .aside -->
@include('back.layout.sidebar')
<!-- /.aside -->
<section id="content">
<section class="vbox">
<section class="scrollable padder">
<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
<li><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
<li class="active">@yield('title')</li>
</ul>
<div class="m-b-md"> <h3 class="m-b-none">@yield('title')</h3> </div>

@yield('content')

</section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
</section>
</section>
</section>

{{-- Footer --}}
@include('back.layout.footer')
{{-- /Footer --}}
