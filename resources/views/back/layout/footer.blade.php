</section>
<!-- Bootstrap -->
<!-- App -->
<script src="{{asset('assets/js/app.v1.js')}}"></script>
<script src="{{asset('assets/js/charts/easypiechart/jquery.easy-pie-chart.js')}}"></script>
<script src="{{asset('assets/js/charts/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/js/charts/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/js/charts/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('assets/js/charts/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('assets/js/charts/flot/jquery.flot.grow.js')}}"></script>
<script src="{{asset('assets/js/charts/flot/demo.js')}}"></script>
<script src="{{asset('assets/js/calendar/bootstrap_calendar.js')}}"></script>
<script src="{{asset('assets/js/calendar/demo.js')}}"></script>
<script src="{{asset('assets/js/sortable/jquery.sortable.js')}}"></script>
<script src="{{asset('assets/js/app.plugin.js')}}"></script>
@yield('script')

</body>

</html>
