<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
<section class="vbox">
<section class="w-f scrollable">
<div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
<!-- nav -->
<nav class="nav-primary hidden-xs">
<ul class="nav">
<li class="active"> <a href="{{url('/dashboard')}}" class="active"> <i class="fa fa-dashboard icon"> <b class="bg-danger"></b> </i> <span>Dashboard</span> </a> </li>
<li> <a href="{{url('/county')}}"> <i class="fa fa-pencil icon"> <b class="bg-info"></b> </i> <span>County</span> </a> </li>
</ul>
</nav>
<!-- / nav -->
</div>
</section>
<footer class="footer lt hidden-xs b-t b-dark">
<a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
<div class="btn-group hidden-nav-xs">
<button type="button" title="Logout" class="btn btn-icon btn-sm btn-dark"><i class="fa fa-power-off"></i></button>
</div>
</footer>
</section>
</aside>
