<header class="bg-dark dk header navbar navbar-fixed-top-xs">
	<div class="navbar-header aside-md">
		<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
			<i class="fa fa-bars"></i>
		</a>
		<a href="#" class="navbar-brand" data-toggle="fullscreen">
			<img src="{{asset('assets/images/logo.png')}}" class="m-r-sm">W.A.H.I
		</a>
		<a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
			<i class="fa fa-cog"></i>
		</a>
	</div>
	<ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
		<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="{!! asset('assets/images/avatar.jpg') !!}"> </span> {{ Auth::user()->name }} <b class="caret"></b> </a>
			<ul class="dropdown-menu animated fadeInRight"> <span class="arrow top"></span>
				<li> <a href="{{ url('user') }}/{{ Auth::user()->slug }}/edit">Profile</a> </li>
				<li class="divider"></li>
				<li> <a class="dropdown-item" href="{{ route('logout') }}"
					onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</li>
		</ul>
	</li>
</ul>
</header>
