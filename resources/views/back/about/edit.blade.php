@extends('back.layout.main')
@section('title', 'Update About')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Update form</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['about.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal']) !!}

@include('back.about._partials.form', ['btntext'=>'Update'])

</form>
</div>
</section>

@endsection
