@extends('back.layout.main')
@section('title', 'About')
@section('content')

  @if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
  @endif

  <section class="panel panel-default">
    <table class="table table-striped m-b-none">
      <thead>
        <tr>
          <th>About</th>
          <th width="70"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          @if (!empty($data) && $data->count())
            @foreach ($data as $key => $value)
              <tr>
                <td> {!! $value->content !!} </td>
                <td>  <a href="{{route('about.edit', $value->id)}}"> <i class="fa fa-edit"></i> </a> </td>
              </tr>
            @endforeach
          @else
            <tr class="table-danger" align="center">
              <td colspan="4" style="color:#ff0000">
                <div class="alert alert-danger">
                  <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                  <a href="{{route('about.create')}}" class="alert-link">Add About the Organization</a>.
                </div>
              </td>
            </tr>
          @endif
        </tr>
      </tbody>
    </table>
  </section>

@endsection
