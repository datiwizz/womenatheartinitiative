@extends('back.layout.main')
@section('title', 'Category')
@section('content')

  @if (session('status'))
  <div class="alert alert-success">
  	{{ session('status') }}
  </div>
  @endif

  <section class="panel panel-default">
    <header class="panel-heading"><a href="{{route('category.create')}}" class="btn btn-s-md btn-info btn-sm">New Category</a></header>
    <table class="table table-striped m-b-none">
      <thead>
        <tr>
          <th>#</th>
          <th>Category</th>
          <th width="70"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          @if (!empty($data) && $data->count())
            @foreach ($data as $key => $value)
              <tr>
                <td> {{++$key}} </td>
                <td> {!! $value->name !!} </td>
                <td>  <a href="{{route('category.edit', $value->id)}}"> <i class="fa fa-edit"></i> </a> </td>
              </tr>
            @endforeach
          @else
            <tr class="table-danger" align="center">
              <td colspan="4" style="color:#ff0000">
                <div class="alert alert-danger">
                  <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                  <a href="{{route('category.create')}}" class="alert-link">Add a Category</a>.
                </div>
              </td>
            </tr>
          @endif
        </tr>
      </tbody>
    </table>
  </section>

@endsection
