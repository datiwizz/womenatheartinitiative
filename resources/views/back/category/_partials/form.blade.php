<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Category</label>
  <div class="col-lg-10">
    {!! Form::text('name', $data->name, array('class' => 'form-control', 'placeholder'=>'Category Name')) !!}
    @if($errors->has('name'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('name') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('category.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>
