@extends('back.layout.main')
@section('title', 'New Category')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">
{!! Form::open(['method'=>'DELETE', 'route'=>['category.destroy',$data->id]]) !!}
<button data-toggle="tooltip" data-placement="top" title="Delete" type="submit" class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this item?');">Delete</button>
{!! Form::close() !!}
Update form</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['category.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal']) !!}

@include('back.category._partials.form', ['btntext'=>'Update'])

</form>
</div>
</section>

@endsection
