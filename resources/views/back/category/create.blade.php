@extends('back.layout.main')
@section('title', 'New Category')
@section('content')

  <section class="panel panel-default">
  <header class="panel-heading font-bold">Create form</header>
  <div class="panel-body">
  {!! Form::open(['route' => 'category.store', 'method' => 'POST', 'class'=>'bs-example form-horizontal']) !!}

  @include('back.category._partials.form', ['btntext'=>'Create'])

  </form>
  </div>
  </section>

@endsection
