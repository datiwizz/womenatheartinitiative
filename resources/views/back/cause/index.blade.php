@extends('back.layout.main')
@section('title', 'Cause')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <header class="panel-heading"><a href="{{route('cause.create')}}" class="btn btn-s-md btn-info btn-sm">New Cause</a></header>
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th>#</th>
        <th>Title</th>
        <th>Creator</th>
        <th>County</th>
        <th>Amount</th>
        <th>Raised</th>
        <th>Status</th>
        <th width="70"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @if (!empty($data) && $data->count())
          @foreach ($data as $key => $value)
            <tr>
              <td> {{ $key + $data->firstItem() }} </td>
              <td> <a href="/cause/{{ $value->slug }}">{!! $value->title !!}</a> </td>
              <td> {!! $value->user->name !!} </td>
              <td> {!! $value->county->name !!} </td>
              <td> {!! $value->amount !!} </td>
              <td> {!! $value->raised !!} </td>
              <td style="font-weight:bold;">
								@if ($value->status === 1)
								<span style="color: #090;">Active</span>
              @elseif ($value->status === 0)
								<span style="color: #f9243f;">Inactive</span>
								@endif
							</td>
              <td>  <a href="{{route('cause.edit', $value->slug)}}"> <i class="fa fa-edit"></i> </a> </td>
            </tr>
          @endforeach
        @else
          <tr class="table-danger" align="center">
            <td colspan="6" style="color:#ff0000">
              <div class="alert alert-danger">
                <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Data found, Please......
                <a href="{{route('cause.create')}}" class="alert-link">Add a Cause</a>.
              </div>
            </td>
          </tr>
        @endif
      </tr>
    </tbody>
  </table>
</section>
<div class="text-right text-center-xs">
{!! $data->render() !!}
</div>
@endsection
