@extends('back.layout.main')
@section('title', 'Create Cause')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['action'=>'CauseController@store', 'method' =>'POST', 'class'=>'bs-example form-horizontal', 'files'=>true]) !!}

@include('back.cause._partials.form', ['btntext' => 'Create'])

</form>
</div>
</section>

@endsection
