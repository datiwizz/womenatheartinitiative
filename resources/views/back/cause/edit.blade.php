@extends('back.layout.main')
@section('title', 'Edit Cause')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">
  {!! Form::open(['method'=>'DELETE', 'route'=>['cause.destroy',$data->id]]) !!}
  <button data-toggle="tooltip" data-placement="top" title="Delete" type="submit" class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this cause?');">Delete</button>
  {!! Form::close() !!}
  Update form</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['cause.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal', 'files'=>true]) !!}

@include('back.cause._partials.form', ['btntext' => 'Update'])

</form>
</div>
</section>

@endsection
