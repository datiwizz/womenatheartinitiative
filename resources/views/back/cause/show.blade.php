@extends('back.layout.main')
@section('title', 'View Cause')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">
<a href="{!! route('cause.index') !!}" class="btn btn-xs btn-info pull-right">Back to cause</a> {!! $data->title !!}
</header>
<div class="panel-body">
<div class="col-lg-3">
<section class="panel panel-default">
<div class="panel-body">
<img class='img-responsive' src="{{ asset('images/cause/'.$data->smallpic) }}" alt="Cause Image" width="100%;" height="auto;">
</div>
</section>
</div>
<div class="col-lg-9">
<section class="panel panel-default">
<div class="panel-body">
<p class="m-t m-b">
Published date: <strong>{!! $data->created_at !!}</strong><br> 
Post status: @if ($data->status === 1)
<span class="label bg-success">Active</span>
@elseif ($data->status === 0)
<span class="label bg-danger">Inactive</span>
@endif <br> 
Created By: <strong>{!! $data->user->name !!}</strong><br> 
County: <strong>{!! $data->county->name !!}</strong><br>
Amount Needed: <strong>{!! $data->amount!!}</strong><br>
Amount Raised: <strong>{!! $data->raised !!}</strong>
</p>
<div class="line"></div>
<p>
{!! $data->content !!}
</p>
</div>
</section>
</div>
</div>
</section>

@endsection
