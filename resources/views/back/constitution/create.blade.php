@extends('back.layout.main')
@section('title', 'Create Constitution')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['route' => 'constitution.store', 'method' => 'POST', 'class'=>'bs-example form-horizontal']) !!}

@include('back.constitution._partials.form', ['btntext'=>'Create'])

</form>
</div>
</section>

@endsection
