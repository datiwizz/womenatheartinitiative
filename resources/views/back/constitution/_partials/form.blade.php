<div class="form-group {{ $errors->has('content') ? 'has-error' :'' }}">
<label class="col-sm-2 control-label">Constitution</label>
<div class="col-lg-10">
    {!! Form::textarea('content', $data->content, array('class' => 'form-control', 'id' => 'notepad')) !!}
    @if($errors->has('content'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('content') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('constitution.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>

@section('style')
<link rel="stylesheet" href="{{ asset('assets/summer/summernote.css') }}" type="text/css">
@endsection
@section('script')
<script src="{{ asset('assets/summer/summernote.js') }}"></script>
<script src="{{ asset('assets/summer/notepad.js') }}"></script>
@endsection
