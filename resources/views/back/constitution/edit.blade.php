@extends('back.layout.main')
@section('title', 'Update Constitution')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Update form</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['constitution.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal']) !!}

@include('back.constitution._partials.form', ['btntext'=>'Update'])

</form>
</div>
</section>

@endsection
