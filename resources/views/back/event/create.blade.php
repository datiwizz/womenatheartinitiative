@extends('back.layout.main')
@section('title', 'Create Event')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">Create form</header>
<div class="panel-body">
{!! Form::open(['action'=>'EventController@store', 'method' =>'POST', 'class'=>'bs-example form-horizontal', 'files'=>true]) !!}

@include('back.event._partials.form', ['btntext' => 'Create'])

</form>
</div>
</section>

@endsection

