<div class="form-group {{ $errors->has('title') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Title</label>
  <div class="col-lg-10">
    {!! Form::text('title', $data->title, array('class' => 'form-control', 'placeholder'=>'Title Name')) !!}
    @if($errors->has('title'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('title') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('event_date') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Event Date</label>
  <div class="col-lg-10">
    {!! Form::text('event_date', $data->event_date, array('class' => 'datepicker-input form-control', 'data-date-format'=>'yyyy-mm-dd', 'placeholder'=>'Event Date')) !!}
    @if($errors->has('event_date'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('event_date') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('county_id') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">County</label>
  <div class="col-lg-10">
    {!! Form::select('county_id', $county, $data->county_id, ['placeholder' => 'Pick a County...', 'class' => 'form-control']) !!}
    @if($errors->has('county_id'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('county_id') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $data->status, ['placeholder' => 'Pick a Status...', 'class'=>'form-control']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group {{ $errors->has('post_image') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">File input</label>
    <div class="col-sm-10">
      {!! Form::file('post_image', [ 'class' => 'form-control']) !!}
      @if($errors->has('post_image'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('post_image') }}</small>
      </span>
    @endif()
    </div>
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Description</label>
  <div class="col-lg-10">
    {!! Form::textarea('content', $data->content, array('class' => 'form-control', 'id' => 'notepad')) !!}
    @if($errors->has('content'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('content') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('event.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>

@section('style')
<link rel="stylesheet" href="{{ asset('assets/summer/summernote.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('assets/js/datepicker/datepicker.css') }}" type="text/css" />
@endsection
@section('script')
<script src="{{ asset('assets/summer/summernote.js') }}"></script>
<script src="{{ asset('assets/summer/notepad.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
