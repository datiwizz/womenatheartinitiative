@extends('back.layout.main')
@section('title', 'Message')
@section('content')

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <div class="box-body no-padding">
    <div class="table-responsive mailbox-messages">
      <table class="table table-hover table-striped">
        <tbody>
          <tr>
            @if(!empty($data) && $data->count())
            @foreach($data as $key => $value)
            <tr>
              <td><input type="checkbox"></td>
              <td class="mailbox-star"><a href="{{route('contact.edit', $value->id)}}"> {!! $value->name !!}</a></td>
              <td class="mailbox-subject"><a href="{{route('contact.edit', $value->id)}}"><b>{!! str_limit($value->title, 35) !!}</b> - {!! str_limit($value->content, 70) !!}</a></td>
              <td class="mailbox-attachment">
                @if ($value->status === 1)
                <span><i class="fa fa-eye"></i></span>
                @elseif ($value->status === 0)
                <span style="color: #090;"><i class="fa fa-eye-slash"></i></span>
                @endif
              </td>
              <td class="mailbox-date">
                @if ($value->created_at->diffInMonths(Carbon::now()) >= 1)
                {{ $value->created_at->format('j M Y , g:ia') }}
                @else
                {{ $value->created_at->diffForHumans() }}
                @endif
              </td>
            </tr>
            @endforeach
            @else
            <tr class="table-danger" align="center">
              <td colspan="6" style="color:#ff0000">
                <div class="alert alert-danger">
                  <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Message received yet. <i class="fa fa-frown-o"></i>
                </div>
              </td>
            </tr>
            @endif
          </tr>
        </tbody>
      </table><!-- /.table -->
    </div><!-- /.mail-box-messages -->
  </div><!-- /.box-body -->

  <div class="box-footer no-padding">
    @if(!empty($data) && $data->count())
    <div class="pull-right">
      {{ $data->links() }}
    </div>
    @else
    @endif
  </div>


</section>


@endsection
