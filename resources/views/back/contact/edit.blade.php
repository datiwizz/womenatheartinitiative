@extends('back.layout.main')
@section('title', 'Read Message')
@section('content')

<section class="panel panel-default">
<header class="panel-heading font-bold">
{!! Form::open(['method'=>'DELETE', 'route'=>['contact.destroy',$data->id]]) !!}
<button data-toggle="tooltip" data-placement="top" title="Delete" type="submit" class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this message?');">Delete</button>
{!! Form::close() !!}
Read Message</header>
<div class="panel-body">
{!! Form::model($data, ['route' => ['contact.update', $data->id], 'method' => 'PATCH', 'class'=>'bs-example form-horizontal']) !!}

@include('back.contact._partials.form', ['btntext'=>'Update Status'])

</form>
</div>
</section>

@endsection
