<div class="form-group">
    <label class="col-lg-2 control-label">Name</label>
    <div class="col-lg-10">
        <p class="form-control-static">{!! $data->name !!}</p>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label">Email</label>
    <div class="col-lg-10">
        <p class="form-control-static">{!! $data->email !!}</p>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label">Subject</label>
    <div class="col-lg-10">
        <p class="form-control-static">{!! $data->title !!}</p>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label">Message</label>
    <div class="col-lg-10">
        <p class="form-control-static">{!! $data->content !!}</p>
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Unread', '1' => 'Read'], $data->status, ['class'=>'form-control']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('contact.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>
