<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
  <label class="col-lg-2 control-label">Email</label>
  <div class="col-lg-10">
    {!! Form::email('email', $data->email, array('class' => 'form-control', 'placeholder'=>'Email Address', 'readonly')) !!}
    @if($errors->has('email'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('email') }}</small>
      </span>
    @endif()
  </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
  <label class="col-sm-2 control-label">Status</label>
  <div class="col-lg-10">
    {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $data->status, ['class'=>'form-control']); !!}
    @if($errors->has('status'))
      <span class="help-block">
        <small style="color: red;">{{ $errors->first('status') }}</small>
      </span>
    @endif()
</div>
</div>
<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
	<a href="{!! route('newsletter.index') !!}" class="btn btn-sm btn-default">Back</a>
	<button type="submit" class="btn btn-sm btn-info pull-right">{{ $btntext }}</button>
</div>
</div>
