@extends('back.layout.main')
@section('title', 'Newsletter Subscribers')
@section('content')

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

<section class="panel panel-default">
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th>#</th>
        <th>Email</th>
        <th>Status</th>
        <th width="70"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @if (!empty($data) && $data->count())
          @foreach ($data as $key => $value)
            <tr>
              <td> {{ $key + $data->firstItem() }} </td>
              <td> {!! $value->email !!} </td>
              <td style="font-weight:bold;">
								@if ($value->status === 1)
								<span style="color: #090;">Active</span>
              @elseif ($value->status === 0)
								<span style="color: #f9243f;">Inactive</span>
								@endif
							</td>
              <td>  <a href="{{route('newsletter.edit', $value->id)}}"> <i class="fa fa-edit"></i> </a> </td>
            </tr>
          @endforeach
        @else
          <tr class="table-danger" align="center">
            <td colspan="6" style="color:#ff0000">
              <div class="alert alert-danger">
                <i class="fa fa-ban-circle"></i><strong>Oh snap!</strong> No Subscribers yet......
              </div>
            </td>
          </tr>
        @endif
      </tr>
    </tbody>
  </table>
</section>
<div class="text-right text-center-xs">
{!! $data->links() !!}
</div>
@endsection

